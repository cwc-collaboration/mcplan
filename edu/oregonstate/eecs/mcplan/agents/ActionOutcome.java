package edu.oregonstate.eecs.mcplan.agents;

import edu.oregonstate.eecs.mcplan.domains.freecell.FreeCellAction;

/**
 * Created by mislam1 on 3/15/16.
 */
public class ActionOutcome {
    FreeCellAction action;
    double reward;
    public ActionOutcome(FreeCellAction fca, double reward) {
        this.action = fca;
        this.reward = reward;
    }
    public FreeCellAction getAction () {
        return action;
    }
    public double getReward () {
        return reward;
    }
}
