package edu.oregonstate.eecs.mcplan.agents;

import edu.oregonstate.eecs.mcplan.Agent;
import edu.oregonstate.eecs.mcplan.Simulator;
import edu.oregonstate.eecs.mcplan.State;

import java.util.List;

public class RandomAgent extends Agent {
    public RandomAgent() {
        name_ = "Random";
    }

    @Override
    public <S extends State, A> A selectAction(S state, Simulator<S, A> iSimulator) {
    	iSimulator.setState(state);
        List<A> actions = iSimulator.getLegalActions();

        //FreeCellAction fca = (FreeCellAction) actions.get(0);
        //if (fca.getActionTypeId() == 1 || fca.getActionTypeId() == 2) return actions.get(0);
        int selectedAction = (int) (Math.random() * actions.size());
        //System.out.println("Rand Selected Action: " + selectedAction + " out of " + actions.size());
        return actions.get(selectedAction);
    }

    @Override
    public <S extends State, A> List<ActionOutcome> selectActions(S state, Simulator<S, A> iSimulator) {
        return null;
    }
}
