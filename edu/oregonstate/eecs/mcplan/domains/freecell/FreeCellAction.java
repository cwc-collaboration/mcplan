package edu.oregonstate.eecs.mcplan.domains.freecell;


import algs.model.searchtree.IMove;
import algs.model.searchtree.INode;

/**
 * Created by mislam1 on 1/25/16.
 */
public abstract class FreeCellAction implements IMove {
    public int actionTypeId = -1;//1,2,3,4,5
    public boolean actionSourcePlanner = true;
    private int noOfCard;
    private String cardString;
    private String srcString;
    private String dstString;

    public String getSemanticString() {
        return semanticString;
    }

    public void setSemanticString(String semanticString) {
        this.semanticString = semanticString;
    }

    private String semanticString;


    public String getCardString() {
        return cardString;
    }

    public void setCardString(String cardString) {
        this.cardString = new String(cardString);
    }

    public String getSrcString() {
        return srcString;
    }

    public void setSrcString(String srcString) {
        this.srcString = new String(srcString);
    }

    public String getDstString() {
        return dstString;
    }

    public void setDstString(String dstString) {
        this.dstString = new String(dstString);
    }


    @Override
    public boolean execute(INode iNode) {
        return false;
    }

    @Override
    public boolean undo(INode iNode) {
        return false;
    }

    @Override
    public boolean isValid(INode iNode) {
        return false;
    }
    public int getActionTypeId(){
        return this.actionTypeId;
    }
    public void setActionSourcePlanner (boolean source) {
        this.actionSourcePlanner = false;
    }
    public boolean getActionSourcePlanner() {
        return this.actionSourcePlanner;
    }

    public String getRankString (int str) {

        if (str == 1) {
            return new String("ace");
        }
        else if (str == 2) {
            return new String("two");
        }
        else if (str == 3) {
            return new String("three");
        }
        else if (str == 4) {
            return new String("four");
        }
        else if (str == 5) {
            return new String("five");
        }
        else if (str == 6) {
            return new String("six");
        }
        else if (str == 7) {
            return new String("seven");
        }
        else if (str == 8) {
            return new String("eight");
        }
        else if (str == 9) {
            return new String("nine");
        }
        else if (str == 10) {
            return new String("ten");
        }
        else if (str == 11) {
            return new String("jack");
        }
        else if (str == 12) {
            return new String("queen");
        }
        else if (str == 13) {
            return new String("king");
        }
        return "";

    }

    public String getSuit (int card) {
        String suits = "CDHS";
        int ss = ((card-1)%4);
        if (ss < 0) {
            char x = 'A';
        }
        char suitChar = suits.charAt(ss);
        if (suitChar == 'C') {
            return new String("clubs");
        }
        else if (suitChar == 'D') {
            return new String("diamonds");
        }
        else if (suitChar == 'H') {
            return new String("hearts");
        }
        else if (suitChar == 'S') {
            return new String("spades");
        }
        return "";
    }
    public String getSuit (int card, boolean flag) {
        String suits = "CDHS";
        if (card >=0 && card < 4) {
            char suitChar = suits.charAt(card);
            if (suitChar == 'C') {
                return new String("clubs");
            } else if (suitChar == 'D') {
                return new String("diamonds");
            } else if (suitChar == 'H') {
                return new String("hearts");
            } else if (suitChar == 'S') {
                return new String("spades");
            }
        }
        return "";
    }
    public String getCardSourceString (int source) {
        if (source >= 0) {
            String string  = "";
            if (source == 0) {
                string = "first";
            } else if (source == 1) {
                string = "second";
            } else if (source == 2) {
                string = "third";
            } else if (source == 3) {
                string = "fourth";
            } else if (source == 4) {
                string = "fifth";
            } else if (source == 5) {
                string = "sixth";
            } else if (source == 6) {
                string = "seventh";
            } else if (source == 7) {
                string = "eighth";
            }
            return " the " +  string +" stack ";
        }
        else if (source == -1) {
            return " the freecell";
        }
        else return "";
    }
    public String getCardSourceString (int source, boolean showColumn) {
        if (source >= 0) {
            String string  = "";
            if (source == 0) {
                string = "first";
            } else if (source == 1) {
                string = "second";
            } else if (source == 2) {
                string = "third";
            } else if (source == 3) {
                string = "fourth";
            } else if (source == 4) {
                string = "fifth";
            } else if (source == 5) {
                string = "sixth";
            } else if (source == 6) {
                string = "seventh";
            } else if (source == 7) {
                string = "eighth";
            }

            return " the " +  string +" stack ";
        }
        else if (source == -1) {
            return " the freecell";
        }
        else return "";
    }

    public String getCardDestinationString (int destination) {
        if (destination == -1) {
            return " the freecell";
        }
        else if (destination == -2) {
            return " a homecell";
        }
        else if (destination >=0 ) {
            String string  = "";
            if (destination == 0) {
                string = "first";
            } else if (destination == 1) {
                string = "second";
            } else if (destination == 2) {
                string = "third";
            } else if (destination == 3) {
                string = "fourth";
            } else if (destination == 4) {
                string = "fifth";
            } else if (destination == 5) {
                string = "sixth";
            } else if (destination == 6) {
                string = "seventh";
            } else if (destination == 7) {
                string = "eighth";
            }

            return " the " +  string +" stack ";// + (destination + 1) + " ";
        }

        else return "";
    }
    public String getCardDestinationString (int destination, boolean showColumn) {
        if (destination == -1) {
            return " the freecell";
        }
        else if (destination == -2) {
            return " a homecell";
        }
        else if (destination >=0 ) {
            String string  = "";
            if (destination == 0) {
                string = "first";
            } else if (destination == 1) {
                string = "second";
            } else if (destination == 2) {
                string = "third";
            } else if (destination == 3) {
                string = "fourth";
            } else if (destination == 4) {
                string = "fifth";
            } else if (destination == 5) {
                string = "sixth";
            } else if (destination == 6) {
                string = "seventh";
            } else if (destination == 7) {
                string = "eighth";
            }

            return " the " +  string +" stack ";// + (destination + 1) + " ";
        }

        else return "";
    }
    //how many cards we are going to transfer with this action
    public abstract int cardCount();
    public abstract int cardSource();
    public abstract int cardDestination();
    public abstract int cardSuit();
    public abstract int cardRank();
    public abstract int cardColor();
    public abstract String getSemanticRep();
}
