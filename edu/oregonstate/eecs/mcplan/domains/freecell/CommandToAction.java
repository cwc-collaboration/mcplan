package edu.oregonstate.eecs.mcplan.domains.freecell;

import edu.oregonstate.eecs.mcplan.Simulator;

/**
 * Created by mislam1 on 2/19/16.
 */
public class CommandToAction {

    private static String move = "move";
    private Simulator<FreeCellNode, FreeCellAction> world_;
    static final String ranks = ".A23456789TJQK";
    static final String suits = "CDHS";
    public void getAction (String command, Simulator simulator) {
        String tempString  = "";

        if (command.contains(move)) {
            String[] splitted = command.split(" ");

            //detect the source column
            FreeCellNode tempState = (FreeCellNode) simulator.getState();
            String sourceCard = splitted[1];
            String destinationCard = splitted[2];

            Column[] cols = tempState.cols;
            for (int i = 0; i < cols.length; ++i) {
                Column col = cols[i];
                int j = 0;
                while (col.cards[j] > 0 && j < 20) {
                    j++;
                }

            }
            tempString = command.replace(move,"").replace("(","").replace(")","");

        }
    }

    public int encodeStringToCard (String card) {
        int suit = card.charAt(0);
        int suitIndex = -1;
        for (int i = 0; i < 4; ++i) {
            if (suits.charAt(i) - 'A' == suit - 'a') {
                suitIndex = i;
                break;
            }
        }
        int rank = -1;
        int rankIndex = -1;
        if (card.length() > 2) {
            rank = (card.charAt(1) - '0') * 10 + (card.charAt(2) - '0');
        }
        else {
            rank = (card.charAt(1) - 'a');
            rankIndex = ranks.indexOf(card.charAt(1));

        }

        int cardInt = (rank - 1) * 4 + 1;
        cardInt += suitIndex;
        //int cardIntN = (sui)

        return  cardInt;
    }
    public static void main (String[] args) {
        CommandToAction action = new CommandToAction();
        action.encodeStringToCard("h12");

    }
}
