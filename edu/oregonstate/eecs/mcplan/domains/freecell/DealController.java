package edu.oregonstate.eecs.mcplan.domains.freecell;

import algs.model.searchtree.IMove;
import edu.oregonstate.eecs.mcplan.History;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

/**
 * Controller to process new deal.
 * 
 * @author George Heineman
 */
public class DealController implements ActionListener {

	/** Frame in which app runs. */
	final JFrame frame;
	
	/** Viewer to represent game on screen. */
	final FreeCellDrawing drawer;
	
	/** List containing moves. */
	final JList list;
	
	/** Last listener (so we can delete with each redeal). */
	StateModifier listener;
	History<FreeCellNode, FreeCellAction> history_;
	
	public DealController (JFrame frame, FreeCellDrawing drawer, JList list, History<FreeCellNode, FreeCellAction> history) {
		this.frame = frame;
		this.drawer = drawer;
		this.list = list;
		this.history_ = history;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int dealNumber;
	
		try {
			dealNumber = Integer.valueOf(e.getActionCommand());
		
			if (dealNumber < 1 || dealNumber > 32000) {
				frame.setTitle("Invalid deal Number (1-32000): " + e.getActionCommand());
				return;
			}
		} catch (NumberFormatException nfe) {
			frame.setTitle("Invalid deal Number (1-32000): " + e.getActionCommand());
			return;
		}
		frame.setTitle("Solution for " + dealNumber);
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
		// simple solution: 28352
		// no solution: 25382
		// infinite loop: 1200
		//int dealNumber = 28352;  
		
		// start from initial state (and make copy for later). 
		/*FreeCellNode fcn;
		try {
			fcn = Deal.initialize(new File ("32000.txt"), dealNumber);
		} catch (IOException e1) {
			frame.setTitle ("Unable to find file \"32000.txt\"");
			return;
		}*/
		
		// Compute the solution.


		Stack<IMove> st = new Stack<>();
		for (int i = 0; i < history_.getSize(); ++i) {
			st.push(history_.getAction(i));
		}
		FreeCellNode fcn;
		fcn = history_.getState(0);
		if (st == null) {
			System.err.println("No Computed Solution for " + dealNumber);
			System.exit(1);
		}
		
		
		DefaultListModel dlm = new DefaultListModel();
		while (!st.isEmpty()) {
			dlm.add(0, st.pop());
		}
		
		list.setModel(dlm);
		list.setSelectedIndex(0);
		if (listener != null) {
			list.removeListSelectionListener(listener);
		}
		
		listener = new StateModifier(list, fcn, drawer, history_);
		list.addListSelectionListener(listener);
		frame.setCursor(Cursor.getDefaultCursor());
		
		drawer.setNode (fcn);
		drawer.repaint();
	}
}
