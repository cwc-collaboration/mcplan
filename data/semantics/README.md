# README #
**
Quick guide to debugging the chart generator:**

This project is a copy of Ryan's parsing framework, augmented with the LambdaFOL and chartGenerator packages in **discriminative/src**.
In chartGenerator, the main class that runs the parser and generator is **GeneratorTest**. There, you can turn on/off debugging messages (very verbose, but necessary for tracing the generation if needed) and provide it with your own lexicon/test sentence files for testing.

To run, provide GeneratorTest with **two arguments**: <name_of_lexicon_file> <name_of_test_sentence_file>

This will attempt to parse the test sentences, then re-generate the resulting expressions.

Example files provided:
lexicon.txt test_sentences.txt: the original lexicon and test sentences using words from this lexicon.

freecell_lexicon.txt freecell_test_sentences.txt: (old) Freecell lexicon and test sentences.

testlexicon.txt testsuite.txt: (new!) Freecell lexicon and test sentences, as close to Colin's version as possible.

----------------------------------------------------------------

A summary of the other classes in chartGenerator is as follows:

**ChartEdge:** represents a chart edge containing a CCGcat, semantic expression, surface string, and edge score (currently not implemented).

**ChartGenerator:** contains the lexicon used for generation and handles lexical lookup. The idea is that there will be one ChartGenerator object which will handle lexical lookup for all GenerationTargets, which will then hand off a set of matching lexical entries to a GenerationChart (one per target) that runs the generation algorithm.

**GenerationChart:** the class that handles the chart and agenda and applies the generation algorithm. This will take the lexical entries found during lookup and instantiate them (as ChartGenItems), then run the generation algorithm and keep a list of completed edges found during processing.

**LexicalEntry:** a wrapper class that holds information about entries in the lexicon (word, category, and semantics) for use in instantiation at the beginning of generation.

**NF:** normal-form class copied from illinoisParser to overcome package visibility issues.