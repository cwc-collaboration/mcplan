package semantics.parsing;

public abstract class Category {
	
	public abstract String category();
	
	public <C extends Category> boolean isSameCategory(C other) {
		return this.category().equals(other.category());
	}
	
	public String toString() {
		return this.category();
	}

}
