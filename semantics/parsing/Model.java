package semantics.parsing;

import java.io.File;
import java.util.Collection;

public abstract class Model<C extends Category, E extends EquivalenceClass<C>> implements Loadable<Model<C, E>> {

	public abstract LexicalCategoryChartItem<C, E> 
				createLexicalItem(LexicalCategoryCandidate<C> lexCat, Sentence sentence);

	public abstract Collection<ChartItem<C, E>> 
				createUnaryParentItems(Cell<C, E> cell);

	public abstract Collection<ChartItem<C, E>> 
				createBinaryParentItems(Cell<C, E> leftCell, Cell<C, E> rightCell);

	public abstract Collection<ChartItem<C, E>> checkForRootItem(CKYChart<C, E> chart);

	public abstract void initializeCache(Sentence sentence);
	
	public abstract void clearCache();

	public abstract String encodeLexicalCategoryChartItem(LexicalCategoryChartItem<C, E> item);

	public abstract String encodeInternalChartItem(ChartItem<C, E> item);

	public abstract String encodeBackpointer(Backpointer<C, E> bp, IntegerMapping<ChartItem<C, E>> items);

	public abstract ChartItem<C, E> decodeChartItem(String encoding);

	public abstract Backpointer<C, E> decodeBackpointer(String encoding, ChartItem<C, E>[] decodedItems);

/*
	public void initializeTrainingDatastructures() {
	}

	public void clearTrainingDatastructures() {
	}

	public void saveParametersDuringTraining(File dir) {
		// TODO Auto-generated method stub
		
	}

	public void trainOnViterbi(CKYChart<C, E> chart, CKYChart<C, E> gold, LearningRateAgenda learningRate,
			UpdateSchedule<C, E> updateSchedule) {
		// TODO Auto-generated method stub
		
	}

	public void updateParameters() {}
	*/
}
