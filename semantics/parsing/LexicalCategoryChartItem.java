package semantics.parsing;

import java.util.Collection;

public class LexicalCategoryChartItem<C extends Category, E extends EquivalenceClass<C>> 
							extends ChartItem<C, E> {
	
	protected double score;

	public LexicalCategoryChartItem(E equivalenceClass, int index, double score) {
		super(equivalenceClass, index, index);
		this.score = score;
	}

	@Override
	public void addBackpointersFromEquivalentItem(ChartItem<C, E> other) {
		System.err.println("Warning: Lexical category chart item doesn't have backpointers.");
	}

	@Override
	public void addBackpointer(Backpointer<C, E> backpointer) {
		System.err.println("Warning: Lexical category chart item doesn't have backpointers.");
	}
	
	public double viterbiScore() {
		return this.score;
	}

	@Override
	public Collection<Backpointer<C, E>> backpointers() {
		System.err.println("Error: Lexical category chart item doesn't have backpointers! Returning null.");
		return null;
	}

	@Override
	public Class<? extends ChartItemFactory> getFactory() {
		return ChartItemFactory.class;
	}
	
	public void setScore(double newScore) {
		this.score = newScore;
	}
	
}
