package semantics.parsing;

import java.util.Collection;

public abstract class Lexicon<C extends Category> implements Loadable<Lexicon<C>> {
	
	public abstract Collection<CategoryCandidate<C>> getInitialCategories(Sentence sentence);
	
	public abstract Collection<LexicalCategoryCandidate<C>> getLexicalCategories(Sentence sentence, int i);
	
}
