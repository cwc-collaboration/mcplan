package semantics.parsing;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map.Entry;

import semantics.ccg.Normal_Form;


public class SymbolicSemanticParsingModel extends SemanticParsingModel {

	public SymbolicSemanticParsingModel(Normal_Form normalForm) {
		super(normalForm);
	}

	public SymbolicSemanticParsingModel(Normal_Form normalForm, ChartItemFactory<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> ciFactory) {	
		super(normalForm, ciFactory);
	}

	@Override
	protected double scoreLexicalItem(SemanticNormalFormEquivalenceClass lexEC, Sentence sentence, int i) {
		return 0.0;
	}

	@Override
	protected double scoreUnaryBackpointer(ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> parent, UnaryBackpointer<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> bp) {
		return 0.0;
	}

	@Override
	protected double scoreBinaryBackpointer(ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> parent, BinaryBackpointer<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> bp) {
		return 0.0;
	}

	@Override
	public void load(File directory) {
		// FIXME load ciFactory (and lexicon?)
	}

	@Override
	public void save(File directory) {
		// FIXME save ciFactory (and lexicon?)
	}

	@Override
	public Object[] getParametersForFutureInstantiation() {
		return new Object[]{this.normalForm};
	}

	@Override
	public void initializeCache(Sentence sentence) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String encodeLexicalCategoryChartItem(
			LexicalCategoryChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> item) {
		return "IMPLEMENT ENCODER";
	}

	@Override
	public String encodeInternalChartItem(
			ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> item) {
		return "IMPLEMENT ENCODER";
	}

	@Override
	public String encodeBackpointer(Backpointer<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> bp,
			IntegerMapping<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> items) {
		return "IMPLEMENT ENCODER";
	}

	@Override
	public ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> decodeChartItem(String encoding) {
		return null;
	}

	@Override
	public Backpointer<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> decodeBackpointer(
			String encoding, ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>[] decodedItems) {
		return null;
	}

}
