package semantics.parsing;

/**
 * Wrapper class for storing information about a tag set (could be extended
 * to support multiple levels of POS tagging, for instance).
 * 
 * @author ramusa2
 *
 * @param <T>
 */
public abstract class TagSet<T extends Tag> {
	
	public abstract T getTag(String tag);

}
