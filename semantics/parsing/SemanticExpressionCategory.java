package semantics.parsing;

import semantics.ccg.ProducedCCGcat;
import semantics.lambdaFOL.Expression;

public class SemanticExpressionCategory extends Category {
	
	private ProducedCCGcat category;

	private Expression expression;
	
	private volatile int hashCode;
	
	public SemanticExpressionCategory(ProducedCCGcat category, Expression expression) {
		this.category = category;
		this.expression = expression;
	}

	@Override
	public String category() {
		return this.category.catString();
	}
	
	public ProducedCCGcat getProducedCCGcat() {
		return this.category;
	}
	
	public Expression getExpression() {
		return this.expression;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof SemanticExpressionCategory)) {
			return false;
		}
		SemanticExpressionCategory c = (SemanticExpressionCategory) o;
		return this == o
				|| (
						(this.category != null ?  this.category.equals(c.category)
												: c.category == null)
					&& (this.expression != null ? this.expression.isEquivalentTo(c.expression)
												: c.expression == null)
					);
	}
	
	@Override
	public int hashCode() {
		int result = this.hashCode;
		if(result == 0) {
			result = this.category.hashCode();
			result = this.expression != null ? 31 * result + this.expression.hashCode() : result;
			this.hashCode = result; // (cache the new hashCode regardless of how it was generated)
		}
		return result;
	}	

}
