package semantics.parsing;

import java.util.ArrayList;

public class ParsedSentence<C extends Category, E extends EquivalenceClass<C>> {
	
	protected Sentence sentence;
	
	protected ArrayList<ParseTree<C, E>> topKParses;
	
	public ParsedSentence(Sentence parsedSentence, ArrayList<ParseTree<C, E>> topK) {
		this.sentence = parsedSentence;
		this.topKParses = topK;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ParsedSentence parseFailure(Sentence failedSentence) {
		return new ParsedSentence(failedSentence, new ArrayList<ParseTree>());
	}
	
	public Sentence sentence() {
		return this.sentence;
	}
	
	public ArrayList<ParseTree<C, E>> topK() {
		return this.topKParses;
	}
	
	public boolean isFailure() {
		return this.topKParses == null || this.topKParses.isEmpty() || this.topKParses.get(0) == null;
	}
	
	public ParseTree<C, E> viterbi() {
		if(this.isFailure()) {
			return null;
		}
		return this.topKParses.get(0);
	}
}
