package semantics.parsing;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Cell<C extends Category, E extends EquivalenceClass<C>> implements Externalizable {
	
	protected ChartItemSet<C, E> items;
	
	protected int spanStart;
	
	protected int spanEnd;
	
	public Cell(int spanStartIndex, int spanEndIndex) {
		this.spanStart = spanStartIndex;
		this.spanEnd = spanEndIndex;
		this.items = new ChartItemSet<C, E>();
	}
	
	public ChartItem<C, E> addItem(ChartItem<C, E> newItem) {
		ChartItem<C, E> added = this.items.addItem(newItem);
		if(added != newItem) {
			added.addBackpointersFromEquivalentItem(newItem);
		}
		return added;
	}
	
	public ChartItemSet<C, E> items() {
		return this.items;
	}
	
	public ChartItem<C, E> getItem(E equivalenceClass) {
		return this.items.getItem(equivalenceClass);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(this.items);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.items = (ChartItemSet<C, E>) in.readObject();
	}

	public int spanStartIndex() {
		return this.spanStart;
	}

	public int spanEndIndex() {
		return this.spanEnd;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(this.spanStart+", ");
		sb.append(this.spanEnd+"] ");
		for(ChartItem item : this.items.items()) {
			sb.append("\n  "+item);
		}
		return sb.toString();
	}

	public boolean isOnDiagonal() {
		return this.spanStart == this.spanEnd;
	}

}
