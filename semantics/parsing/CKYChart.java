package semantics.parsing;

public class CKYChart<C extends Category, E extends EquivalenceClass<C>> 
		extends Chart<C, E> {
	
	Cell<C, E>[][] cells;

	public CKYChart(Sentence sentenceToParse) {
		super(sentenceToParse);
		this.cells = new Cell[this.sentence.length()][this.sentence.length()];
		int length = this.sentence.length();
		for(int span=0; span<length; span++) {
			for(int i=0; i<length-span; i++) {
				int j = i+span;
				this.cells[i][j] = new Cell<C, E>(i, j);
			}
		}
	}
	
	public Cell<C, E> getCell(int i, int j) {
		return this.cells[i][j];
	}
	
	public boolean successfullyParsed() {
		return this.root() != null;
	}

	public boolean contains(ChartItem item) {
		if(item == null) {
			return false;
		}
		boolean contained = containsItem(item);
		if(item instanceof LexicalCategoryChartItem) {
			return contained;
		}
		if(!contained) {
			return false;
		}
		Backpointer bp = item.viterbi();
		if(bp instanceof UnaryBackpointer) {
			return contained && this.contains(((UnaryBackpointer) bp).child());
		}
		if(bp instanceof BinaryBackpointer) {
			return contained
					&& contains(((BinaryBackpointer) bp).leftChild())
					&& contains(((BinaryBackpointer) bp).rightChild());
		}
		return false;
	}


	private boolean containsItem(ChartItem<C, E> item) {
		int s = item.spanStartIndex();
		int e = item.spanEndIndex();
		Cell<C, E> cell = getCell(s, e);
		E ec = item.getEquivalenceClass();
		ChartItem<C, E> ecExactMatch = cell.getItem(ec);
		if(ecExactMatch != null) {
			return true;
		}		
		for(ChartItem<C, E> cellItem : cell.items().items()) {
			if(cellItem.getEquivalenceClass().hasSameCategory(ec)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adds an item to a cell. If an equivalent item already exists
	 * in the cell, add the item's backpointers to the existing
	 * item's list of children. 
	 */
	public ChartItem<C, E> addItemToChart(ChartItem<C, E> item, Cell<C, E> cell) {
		return this.addItemToChart(item, cell.items());
	}	

	protected ChartItem<C, E> addItemToChart(ChartItem<C, E> item, ChartItemSet<C, E> items) {
		ChartItem<C, E> added = items.addItem(item);
		if(added != item) {
			added.addBackpointersFromEquivalentItem(item);
		}
		return added;
	}

	public boolean contains(CKYChart<C, E> other) {
		if(this.cells.length != other.cells.length) {
			return false;
		}
		int l = cells.length;
		for(int s=0; s<l; s++) {
			for(int i=0; i+s<l; i++) {
				Cell<C, E> mine = this.cells[i][s+i];
				Cell<C, E> theirs = other.cells[i][s+i];
				for(ChartItem<C, E> m : mine.items().items()) {
					if(!theirs.items().contains(m)) {
						return false;
					}
				}
				for(ChartItem<C, E> t : theirs.items().items()) {
					if(!mine.items().contains(t)) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
}
