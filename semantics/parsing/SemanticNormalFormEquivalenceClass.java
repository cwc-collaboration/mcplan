package semantics.parsing;

import semantics.ccg.ProducedCCGcat;
import semantics.ccg.Rule_Type;
import semantics.lambdaFOL.Expression;

public class SemanticNormalFormEquivalenceClass extends 
							CategoryEquivalenceClass<SemanticExpressionCategory> {

	public SemanticNormalFormEquivalenceClass(SemanticExpressionCategory category) {
		super(category);
	}

	public SemanticNormalFormEquivalenceClass(ProducedCCGcat category, Expression expression) {
		this(new SemanticExpressionCategory(category, expression));
	}
	
	public Expression getExpression() {
		return this.category.getExpression();
	}
	
	public ProducedCCGcat getProducedCCGcat() {
		return this.category.getProducedCCGcat();
	}
	
	public int combinatorArity() {
		return this.getProducedCCGcat().getArityOfCombinatorUsed();
	}
	
	public Rule_Type getCombinatorUsed() {
		return this.getProducedCCGcat().getCombinatorUsed();
	}

}
