package semantics.parsing;


public class BinaryBackpointer<C extends Category, E extends EquivalenceClass<C>> 
							extends Backpointer<C, E> {

	protected ChartItem<C, E> leftChild;
	protected ChartItem<C, E> rightChild;
	
	public BinaryBackpointer(ChartItem<C, E> leftChild, ChartItem<C, E> rightChild) {
		super();
		this.leftChild = leftChild;
		this.rightChild = rightChild;
	}

	public BinaryBackpointer(ChartItem<C, E> leftChild, ChartItem<C, E> rightChild, double score) {
		super(score);
		this.leftChild = leftChild;
		this.rightChild = rightChild;
	}
	
	public ChartItem<C, E> leftChild() {
		return this.leftChild;
	}
	
	public ChartItem<C, E> rightChild() {
		return this.rightChild;
	}
}
