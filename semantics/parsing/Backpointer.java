package semantics.parsing;


public abstract class Backpointer<C extends Category, E extends EquivalenceClass<C>> {

	protected double score;
	
	public Backpointer() {
		this.score = Double.NEGATIVE_INFINITY;
	}
	
	public Backpointer(double score) {
		this.score = score;
	}
	
	public void setScore(double newScore) {
		this.score = newScore;
	}
	
	public double score() {
		return this.score;
	}
}
