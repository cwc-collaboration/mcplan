package semantics.parsing;

public enum SentenceAnnotation {
	
	RAW,
	
	POS,
	
	MTAGGED,
	
	STAGGED,
	
	STAGGED_REFORMAT,
	
	PARG,
	
	AUTO

}
