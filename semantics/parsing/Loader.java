package semantics.parsing;

import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Scanner;

public class Loader<T extends Loadable<T>> {

	protected static final String NULL_PARAM = "Object\tnull";

	protected static final String PARAMS_FILE = "loader.params";

	protected final String directoryName;

	public Loader(String directoryName) {
		if(!directoryName.endsWith(File.separator)) {
			directoryName += File.separator;
		}
		this.directoryName = directoryName;
	}

	public void save(T loadableObject) {
		File dir = new File(this.directoryName);
		dir.mkdirs();
		try {
			File typeFile = new File(dir+File.separator+PARAMS_FILE);
			PrintWriter pw = new PrintWriter(typeFile);
			pw.println(loadableObject.getClass().getName());
			Object[] params = loadableObject.getParametersForFutureInstantiation();
			for(Object param : params) {
				pw.println(getParamString(param));
			}
			pw.close();
		}
		catch(Exception e) {
			System.err.println("ERROR: Unable to write type file.");
			e.printStackTrace();
		}
		loadableObject.save(dir);
	}

	@SuppressWarnings("unchecked")
	public <T2 extends T> T load() {
		File dir = new File(this.directoryName);
		File typeFile = new File(dir+File.separator+PARAMS_FILE);
		try {
			Scanner sc = new Scanner(typeFile);
			Class<T2> clazz = (Class<T2>) Class.forName(sc.nextLine());
			ArrayList<Object> paramList = new ArrayList<Object>();
			while(sc.hasNextLine()) {
				String line = sc.nextLine().trim();
				if(!line.isEmpty()) {
					paramList.add(readParamString(line));
				}
			}
			sc.close();
			Object[] params = paramList.toArray(new Object[paramList.size()]);
			Class<? extends Object>[] paramClasses = new Class[params.length];
			for(int c=0; c<params.length; c++) {
				if(params[c] == null) {
					paramClasses[c] = Object.class;
					continue;
				}
				paramClasses[c] = params[c].getClass();
			}
			
			T loadableObject = instantiate(clazz, paramClasses, params);
			if(loadableObject == null) {
				System.err.println("ERROR: Failed to load loadable object of type: "+clazz.getName());
				return null;
			}
			loadableObject.load(dir);
			return loadableObject;
		}
		catch(Exception e) {
			System.err.println("ERROR: Failed to load object; corrupt or unrecognized type file.");
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * TODO
	 * Note: Either Loadable objects must implement empty constructor, or we need to figure a way to include arguments in the class file. 
	 * 
	 * Maybe something like:
	 * 		edu.illinois.cs.nlp.neuralccg. ... .NameOfT2
	 *      int		42
	 *      String	foo
	 *      int		27
	 *      ...
	 *      
	 * Where the additional lines specify parameters for the object constructor?
	 * 
	 * @param clazz
	 * @return
	 */
	protected <T2 extends T> T instantiate(Class<T2> clazz, Class<? extends Object>[] classes,
			Object[] params) {
		try {
			Constructor<T2> ctor = clazz.getConstructor(classes);
			return (T) ctor.newInstance(params);
		}
		catch(NoSuchMethodException e) {
			String err = "ERROR: Class "+clazz+" does not implement "
					+ "a public constructor with the following Object arguments: "
					+"\n       ";
			for(Class<? extends Object> c : classes) {
				err +="           "+c.getName()+"\n";
			}
			err += "       (Tragically, primitives are not supported at this time.)\n";
			
			System.err.println(err);
			return null;
		} catch (Exception e) {
			System.err.println("ERROR: Could not load class "+clazz);
			e.printStackTrace();
			return null;
		}
	}

	protected String getParamString(Object param) {
		if(param instanceof String) {
			return "String\t"+param;
		}
		if(param instanceof Integer) {
			return "Integer\t"+param;
		}
		if(param instanceof Boolean) {
			return "Boolean\t"+param;
		}
		if(param instanceof Double) {
			return "Double\t"+param;
		}
		if(param instanceof Long) {
			return "Long\t"+param;
		}
		if(param.getClass().isEnum()) {
			return param.getClass().getName()+"\t"+param;
		}
		if(param.getClass().equals(File.class)) {
			return "Loadable\t"+((File)param).getPath();
		}
		System.err.println("WARNING: Failed to read parameter: "+param
				+"\n         Unrecognized type: "+param.getClass().getName()
				+"\n         Saving null Object instead.");
		return NULL_PARAM;
	}

	protected Object readParamString(String param) {
		String[] split = param.trim().split("\t");
		if(split.length != 2) {
			System.err.println("WARNING: Failed to read parameter: "+param
					+"\n         Parameter must have two tab-separated fields: type\tvalue"
					+"\n         Instantiating with null instead.");
			return null;
		}
		try {
			String type = split[0];
			String value = split[1];
			if(type.equalsIgnoreCase("String")) {
				return value;
			}
			if(type.equalsIgnoreCase("Integer") || type.equalsIgnoreCase("int")) {
				return Integer.parseInt(value);
			}
			if(type.equalsIgnoreCase("Double")) {
				return Double.parseDouble(value);
			}
			if(type.equalsIgnoreCase("Boolean")) {
				return Boolean.parseBoolean(value);
			}
			if(type.equalsIgnoreCase("Long")) {
				return Long.getLong(value);
			}
			if(type.equalsIgnoreCase("Loadable")) {
				try {
					Loader loader = new Loader(this.directoryName+value);
					return loader.load();
				}
				catch(Exception e) {
					e.printStackTrace();
				}				
			}
			try {
				Class<? extends Enum> enumClass = (Class<? extends Enum>) Class.forName(type);
				return Enum.valueOf(enumClass, value);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			try {
				Class<? extends Loadable> loadableClass = (Class<? extends Loadable>) Class.forName(type);
				Loader loader = new Loader(value);
				return loader.load();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			System.err.println("WARNING: Failed to read parameter: "+param
					+"\n         Unrecognized type: "+type
					+"\n         Instantiating with null instead.");
			return null;
		}
		catch(Exception e) {
			System.err.println("WARNING: Failed to read parameter: "+param
					+"\n         Instantiating with null instead.");
			return null;
		}
	}
	
	public static <L extends Loadable<L>> void save(L toSave, String saveDir) {
		Loader<L> loader = new Loader<L>(saveDir);
		loader.save(toSave);
	}
	
	public static <L extends Loadable<L>> L load(String loadDir) {
		return (L) (new Loader(loadDir)).load();
	}

}
