package semantics.parsing;

import java.io.Externalizable;


public abstract class Parser<C extends Category, E extends EquivalenceClass<C>> 
					implements Loadable<Parser<C, E>> {
	
	protected Parser() {
		
	}
	
	public abstract ParsedSentence<C, E> parse(Sentence sentence);
	
}
