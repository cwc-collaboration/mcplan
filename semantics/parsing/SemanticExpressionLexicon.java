package semantics.parsing;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import semantics.ccg.CCGcat;
import semantics.ccg.ProducedCCGcat;
import semantics.ccg.Rule_Type;
import semantics.lambdaFOL.Expression;
import semantics.lambdaFOL.SemLexicon;


public class SemanticExpressionLexicon extends Lexicon<SemanticExpressionCategory> {

	private SemLexicon lexicon;

	public SemanticExpressionLexicon(String lexiconFile) {
		Expression.readLexicon(lexiconFile);
		this.lexicon = Expression.lexicon;
	}
	public SemanticExpressionLexicon(SemLexicon lexicon) {
		this.lexicon = lexicon;
	}

	@Override
	public void load(File directory) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(File directory) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object[] getParametersForFutureInstantiation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<LexicalCategoryCandidate<SemanticExpressionCategory>> getLexicalCategories(Sentence sentence, int i) {
		ArrayList<LexicalCategoryCandidate<SemanticExpressionCategory>> categories = 
				new ArrayList<LexicalCategoryCandidate<SemanticExpressionCategory>>();
		String word = sentence.wordAt(i);
		HashMap<String, ArrayList<Expression>> entries = lexicon.getAllEntries(word);
		if (entries != null) {
			for(String cat : entries.keySet()) {
				ProducedCCGcat ccgCat = new ProducedCCGcat(Rule_Type.LEXICAL_CATEGORY, -1, 
						CCGcat.lexCat(word, cat, sentence.posTagAt(i), i));
				for (Expression exp : entries.get(cat)) {
					categories.add(
							new LexicalCategoryCandidate<SemanticExpressionCategory>(
									new SemanticExpressionCategory(ccgCat, exp), i));
				}
			}
		}
		return categories;
	}

	@Override
	public Collection<CategoryCandidate<SemanticExpressionCategory>> getInitialCategories(Sentence sentence) {
		Collection<CategoryCandidate<SemanticExpressionCategory>> itemsToAdd = 
				new ArrayList<CategoryCandidate<SemanticExpressionCategory>>();
		for(int i=0; i<sentence.length(); i++) {
			itemsToAdd.addAll(this.getLexicalCategories(sentence, i));
		}
		return itemsToAdd;
	}

}
