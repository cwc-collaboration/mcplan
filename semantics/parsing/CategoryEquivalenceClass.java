package semantics.parsing;


public class CategoryEquivalenceClass<C extends Category> extends EquivalenceClass<C> {
	
	protected C category;
	
	public CategoryEquivalenceClass(C category) {
		this.category = category;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof CategoryEquivalenceClass)) {
			return false;
		}
		CategoryEquivalenceClass<?> eq = (CategoryEquivalenceClass<?>) o;
		return this.category == null ? eq.category == null : this.category.equals(eq.category);
	}
	
	@Override
	public int hashCode() {
		if(this.category == null) {
			return 17;
		}
		return this.category.hashCode();
	}
	
	public C category() {
		return this.category;
	}
	
	@Override
	public String toString() {
		if(this.category == null) {
			return "(null)";
		}
		return this.category.toString();
	}
}
