package semantics.parsing;

import semantics.ccg.CCGcat;

public class ParseTreeNode<C extends Category, E extends EquivalenceClass<C>> {

	protected int spanStart;

	protected int spanEnd;
	
	protected CCGcat ccgcat;

	protected E equivalenceClass;

	protected ParseTreeNode<C, E> leftChild;

	protected ParseTreeNode<C, E> rightChild;

	public ParseTreeNode(CCGcat ccgCat, E equivalenceClass, int index) {
		this(ccgCat, equivalenceClass, null, null, index, index);
	}

	public ParseTreeNode(CCGcat ccgCat, E equivalenceClass, ParseTreeNode<C, E> child) {
		this(ccgCat, equivalenceClass, child, null, child.spanStart, child.spanEnd);
	}

	public ParseTreeNode(CCGcat ccgCat, E equivalenceClass, 
			ParseTreeNode<C, E> leftChild, ParseTreeNode<C, E> rightChild) {
		this(ccgCat, equivalenceClass, leftChild, rightChild, leftChild.spanStart, rightChild.spanEnd);
	}

	protected ParseTreeNode(CCGcat ccgCat, E equivalenceClass, 
			ParseTreeNode<C, E> leftChild, ParseTreeNode<C, E> rightChild, 
			int spanStart, int spanEnd) {
		this.ccgcat = ccgCat;
		this.equivalenceClass = equivalenceClass;
		this.leftChild = leftChild;
		this.rightChild = rightChild;
		this.spanStart = spanStart;
		this.spanEnd = spanEnd;
	}


	public boolean isLeaf() {
		return this.leftChild == null;
	}

	public boolean isUnary() {
		return this.leftChild != null && this.rightChild == null;
	}

	public boolean isBinary() {
		return this.leftChild != null && this.rightChild != null;
	}

	public ParseTreeNode<C, E> child() {
		return this.leftChild;
	}
	
	public ParseTreeNode<C, E> leftChild() {
		return this.leftChild;
	}
	
	public ParseTreeNode<C, E> rightChild() {
		return this.rightChild;
	}
	
	public void setCCGcat(CCGcat ccgCat) {
		this.ccgcat = ccgCat;
	}

	public CCGcat ccgCat() {
		return this.ccgcat;
	}
	
	public String category() {
		return this.ccgcat.catString();
	}
	
	public E equivalenceClass() {
		return this.equivalenceClass;
	}
	
	public int spanStart() {
		return this.spanStart;
	}
	
	public int spanEnd() {
		return this.spanEnd;
	}

}
