package semantics.parsing;

import java.util.Arrays;

/**
 * Stores the words in a sentence as an array of Strings,
 * along with other (optional) annotations such as
 * POS tags, gold AUTO parses/PARG dependencies,
 * gold supertags, supteragger-assigned multitags, etc.
 * 
 * @author ramusa2
 *
 */
public class Sentence {


	/** Mandatory array of word tokens **/
	protected String[] words;

	/** Optional array of POS tags **/
	protected POS[] posTags;

	/** Optional array of gold supertags **/
	protected Supertag[] goldSupertags;

	/**
	 // Ryan: these are used for syntactic parsing
	protected SupertagDistribution[] multitags;
	protected ParseTree<?, ?> goldTree;
	protected DepSet pargDeps;
	protected String autoString;
	*/

	public Sentence(String whiteSpaceDelimitedTokens) {
		this.words = whiteSpaceDelimitedTokens.trim().split("\\s+");
	}
	
	/*
	public Sentence(String whiteSpaceDelimitedTokens) {
		this(whiteSpaceDelimitedTokens.trim().split("\\s+"));
	}

	public Sentence(String[] words) {
		this(words, null, null, null, null);
	}

	public Sentence(String[] words, POS[] posTags) {
		this(words, posTags, null, null, null);
	}

	public Sentence(String[] words, POS[] posTags, Supertag[] goldSupertags, ParseTree<?, ?> goldTree) {
		this(words, posTags, goldSupertags, null, goldTree);
	}

	public Sentence(String[] words, POS[] posTags, SupertagDistribution[] multitags) {
		this(words, posTags, null, multitags, null);
	}

	public Sentence(String[] words, POS[] posTags, Supertag[] goldSupertags) {
		this(words, posTags, goldSupertags, null, null);
	}

	public Sentence(String[] words, POS[] posTags, Supertag[] goldSupertags, 
			SupertagDistribution[] multitags, ParseTree<?, ?> goldTree) {
		this.words = words;
		this.posTags = posTags;
		this.goldSupertags = goldSupertags;
		this.multitags = multitags;
		this.goldTree = null;
		this.pargDeps = null;
		this.autoString = null;
	}
	*/

	public int length() {
		return this.words.length;
	}

	public String wordAt(int i) {
		return this.words[i];
	}

	/**
	 * Returns the whitespace-delimited span of words from i to j, inclusive.
	 */
	public String span(int i, int j) {
		StringBuilder sb = new StringBuilder();
		for(int k=i; k<j; k++) {
			sb.append(this.words[k]);
			sb.append(" ");
		}
		if(j >= i) {
			sb.append(this.words[j]);
		}
		return sb.toString();
	}

	public String toString() {
		return this.span(0, this.length()-1);
	}

	public String[] getWords() {
		return this.words;
	}

	public boolean hasPOSTags() {
		return this.posTags != null;
	}

	public POS[] getPOSTags() {
		return this.posTags;
	}

	public void setPOSTags(POS[] posTags) {
		this.posTags = posTags;
	}

	public String posTagAt(int index) {
		if(this.hasPOSTags()) {
			return this.posTags[index].tag();
		}
		return "NULL";
	}

	/*
	public boolean hasPARGDeps() {
		return this.pargDeps != null;
	}

	public DepSet getPARGDeps() {
		return this.pargDeps;
	}

	public PargDepSet getPARGDepSet() {
		return new PargDepSet(this, this.pargDeps);
	}

	public void setPARGDeps(DepSet pargDeps) {
		this.pargDeps = pargDeps;
	}

	public boolean hasAUTOString() {
		return this.autoString != null;
	}

	public String getAUTOString() {
		return this.autoString;
	}

	public void setAUTOString(String autoString) {
		this.autoString = autoString;
	}

	public boolean hasGoldSupertags() {
		return this.goldSupertags != null;
	}

	public Supertag[] getGoldSupertags() {
		return this.goldSupertags;
	}


	public void setGoldSupertags(Supertag[] goldSupertags) {
		this.goldSupertags = goldSupertags;
	}

	public boolean hasMultitags() {
		return this.multitags != null;
	}

	public SupertagDistribution[] getMultitags() {
		return this.multitags;
	}

	public void setMultitags(SupertagDistribution[] multitags) {
		this.multitags = multitags;
	}

	public boolean hasGoldTree() {
		return this.goldTree != null;
	}

	public ParseTree<?, ?> getGoldTree() {
		return this.goldTree;
	}

	public void setGoldTree(ParseTree<?, ?> goldTree) {
		this.goldTree = goldTree;
	}

	public static Sentence mergeAnnotations(Sentence a, Sentence b) {
		if(a.isDummy() || b.isDummy()) {
			return mergeAddPargDeps(a, b);
		}
		if(a.length() != b.length()) {
			System.err.println("ERROR: Cannot merge sentences of different lengths.");
			return null;
		}
		Sentence merged = new Sentence(a.words);
		if(!Arrays.equals(a.words, b.words)) {
			if(a.hasAUTOString()) {
				merged.words = filterWords(a.words, b.words);
			}
			else if(b.hasAUTOString()) {
				merged.words = filterWords(b.words, a.words);
			}
			if(merged.words == null) {
				System.err.println("ERROR: Cannot merge sentences with different word tokens.");
				return null;
			}
		}
		if(a.hasPOSTags()) {
			merged.posTags = a.posTags;
		}
		else {
			merged.posTags = b.posTags;
		}
		if(a.hasGoldSupertags()) {
			merged.goldSupertags = a.goldSupertags;
		}
		else {
			merged.goldSupertags = b.goldSupertags;
		}
		if(a.hasMultitags()) {
			merged.multitags = a.multitags;
		}
		else {
			merged.multitags = b.multitags;
		}
		if(a.hasGoldTree()) {
			merged.goldTree = a.goldTree;
		}
		else {
			merged.goldTree = b.goldTree;
		}
		if(a.hasPARGDeps()) {
			merged.pargDeps = a.pargDeps;
		}
		else {
			merged.pargDeps = b.pargDeps;
		}
		if(a.hasAUTOString()) {
			merged.autoString = a.autoString;
		}
		else {
			merged.autoString = b.autoString;
		}
		return merged;
	}

	private static String[] filterWords(String[] auto, String[] gold) {
		String[] filtered = new String[auto.length];
		for(int i=0; i<filtered.length; i++) {
			if(auto[i].equals(gold[i])) {
				filtered[i] = auto[i];
				continue;
			}
			if(filterWord(auto[i]).equals(gold[i])) {
				filtered[i] = gold[i];
			}
		}
		return filtered;
	}

	private static String filterWord(String autoWord) {
		if(autoWord.startsWith("-")) {
			if(autoWord.equals("-LRB-")) {
				return "(";
			}
			if(autoWord.equals("-RRB-")) {
				return ")";
			}
			if(autoWord.equals("-LCB-")) {
				return "{";
			}
			if(autoWord.equals("-RCB-")) {
				return "}";
			}
		}
		return autoWord;
	}

	private static Sentence mergeAddPargDeps(Sentence a, Sentence b) {
		if(a.isDummy()) {
			b.pargDeps = a.pargDeps;
			return b;
		}
		if(b.isDummy()) {
			a.pargDeps = b.pargDeps;
			return a;
		}
		return null;
	}

	private boolean isDummy() {
		return this.words.length == 1 && this.words[0].equals(DUMMY);
	}

	protected static final String DUMMY = "DUMMY";

	public static Sentence getDummySentenceToStorePARGDeps(DepSet pargDeps) {
		Sentence sen = new Sentence(DUMMY);
		sen.setPARGDeps(pargDeps);
		return sen;
	}

*/
}
