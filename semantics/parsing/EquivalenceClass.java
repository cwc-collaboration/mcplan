package semantics.parsing;


public abstract class EquivalenceClass<C extends Category> {
	
	public abstract C category();
	
	public <E extends EquivalenceClass<C>> boolean hasSameCategory(E otherEC) {
		return this.category().isSameCategory(otherEC.category());
	}
	
	public String toString() {
		return this.category().toString();
	}
}
