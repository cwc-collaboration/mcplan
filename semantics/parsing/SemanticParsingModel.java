package semantics.parsing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;

import semantics.ccg.CCGcat;
import semantics.ccg.NF;
import semantics.ccg.Normal_Form;
import semantics.ccg.ProducedCCGcat;
import semantics.ccg.Rule_Type;
import semantics.lambdaFOL.Expression;
import semantics.lambdaFOL.LambdaAbstraction;


public abstract class SemanticParsingModel extends Model<SemanticExpressionCategory, 
SemanticNormalFormEquivalenceClass> {

	protected static ArrayList<Rule_Type> SUPPORTED_COMBINATORS;

	static {
		SUPPORTED_COMBINATORS = new ArrayList<Rule_Type>();
		SUPPORTED_COMBINATORS.add(Rule_Type.FW_APPLY);
		SUPPORTED_COMBINATORS.add(Rule_Type.BW_APPLY);
		SUPPORTED_COMBINATORS.add(Rule_Type.FW_COMPOSE);
		SUPPORTED_COMBINATORS.add(Rule_Type.BW_COMPOSE);
	}

	protected Normal_Form normalForm;

	protected ChartItemFactory<SemanticExpressionCategory, 
	SemanticNormalFormEquivalenceClass> ciFactory;

	protected SemanticParsingModel(Normal_Form normalForm) {
		this(normalForm, new ProfligateChartItemFactory<SemanticExpressionCategory, 
				SemanticNormalFormEquivalenceClass>());
	}

	protected SemanticParsingModel(Normal_Form normalForm, ChartItemFactory<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> ciFactory) {	
		this.normalForm = normalForm;
		this.ciFactory = ciFactory;
	}

	@Override
	public LexicalCategoryChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> createLexicalItem(
			LexicalCategoryCandidate<SemanticExpressionCategory> lexCat, Sentence sentence) {
		int i = lexCat.sentencePosition();
		SemanticNormalFormEquivalenceClass lexEC = new SemanticNormalFormEquivalenceClass(lexCat.category());
		return new LexicalCategoryChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>(
				lexEC, i, this.scoreLexicalItem(lexEC, sentence, i));
	}

	protected abstract double scoreLexicalItem(SemanticNormalFormEquivalenceClass lexEC, Sentence sentence, int i);

	protected abstract double scoreUnaryBackpointer(ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> parent, UnaryBackpointer<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> bp);

	protected abstract double scoreBinaryBackpointer(ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> parent, BinaryBackpointer<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> bp);

	@Override
	public Collection<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> createUnaryParentItems(
			Cell<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> cell) {
		ArrayList<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>>
		parentItems = new ArrayList<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>>();
		//		if(cell.isOnDiagonal()) {
		//			this.addUnaryParentItems(cell, Rule_Type.TYPE_CHANGE, parentItems); 
		//			// FIXME: uncomment when semantics for type-changing rules are implemented
		//			//        (also add check for type changing in non-diagonal cells, if desired)
		//		}
		this.addUnaryParentItems(cell, Rule_Type.FW_TYPERAISE, parentItems);
		return parentItems;
	}

	protected void addUnaryParentItems(
			Cell<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> cell,
			Rule_Type combinator,
			ArrayList<ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass>> parentItems) {
		int start = cell.spanStartIndex();
		int end = cell.spanEndIndex();
		for(ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> childItem
				: cell.items().items()) {
			SemanticNormalFormEquivalenceClass childEC = childItem.getEquivalenceClass();
			if(NF.unaryNF(this.normalForm, childEC.getCombinatorUsed(), combinator)) {
				SemanticNormalFormEquivalenceClass parentEC;
				switch(combinator) {
				case FW_TYPERAISE:
					parentEC = typeRaiseFW(childEC.getProducedCCGcat(), childEC.getExpression());
					break;
				case TYPE_CHANGE:
					parentEC = typeChange(childEC.getProducedCCGcat(), childEC.getExpression());
					break;
				default:
					System.err.println("WARNING: Unsupported semantic unary combination: "+combinator);
					continue;
				}
				if(parentEC != null) {
					ChartItem parent = this.ciFactory.internalItem(parentEC, start, end);
					UnaryBackpointer bp = new UnaryBackpointer(childItem);
					bp.setScore(this.scoreUnaryBackpointer(parent, bp));
					parent.addBackpointer(bp);
					parentItems.add(parent);
				}
			}
		}
	}

	private SemanticNormalFormEquivalenceClass typeChange(ProducedCCGcat childCat, Expression childExp) {
		switch (childCat.catString()) {
		case "N":
			ProducedCCGcat result_cat = CCGcat.getTypeChangedWithNormalFormInfo(childCat, "NP");
			Expression result_exp = childExp; // FIXME: produce type-changed expression for this rule (I don't know how to do this yet), it shouldn't just be the old expression
			if(result_cat != null /* FIXME: add exp check if it becomes necessary */) {
				return new SemanticNormalFormEquivalenceClass(result_cat, result_exp);
			}
			break;
		default:
			System.err.println("WARNING: Unsupported child category for semantic type change: "+childCat.catString());
			return null;
		}
		return null;
	}

	private SemanticNormalFormEquivalenceClass typeRaiseFW(ProducedCCGcat childCat, Expression childExpression) {
		ProducedCCGcat result_cat = null;
		switch (childCat.catString()) {
		case "NP": // we're only forward type-raising NP to S/(S\NP)
			result_cat = CCGcat.getTypeRaisedCatWithNormalFormInfo(
					childCat.getProducedCCGcat(), "S/(S\\NP)");
			break;
		default:
			return null;
		}
		if(result_cat != null && result_cat.getCombinatorUsed() == Rule_Type.FW_TYPERAISE) {
			Expression result_exp = childExpression.typeRaise();
			if (result_exp != null) {
				return new SemanticNormalFormEquivalenceClass(result_cat, result_exp);
			}
		}
		return null;
	}

	@Override
	public Collection<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> createBinaryParentItems(
			Cell<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> leftCell,
			Cell<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> rightCell) {
		int start = leftCell.spanStartIndex();
		int end = rightCell.spanEndIndex();
		ArrayList<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> binaryParents = 
				new ArrayList<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>>();
		for (Entry<SemanticNormalFormEquivalenceClass, 
				ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> left : leftCell.items().mapEntries()) {
			SemanticNormalFormEquivalenceClass leftEC = left.getKey();
			ProducedCCGcat leftCat = leftEC.getProducedCCGcat();
			//LambdaAbstraction leftExp = (LambdaAbstraction)leftEC.getExpression();
			Expression leftExp = leftEC.getExpression();
			ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> leftItem = left.getValue();

			for (Entry<SemanticNormalFormEquivalenceClass, 
					ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> right : rightCell.items().mapEntries()) {
				SemanticNormalFormEquivalenceClass rightEC = right.getKey();
				ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> rightItem = right.getValue();
				// Get possible parent cats based on children, supported combinators, and normal form constraints
				ArrayList<ProducedCCGcat> result_cats = CCGcat.combineCheckNormalForm(this.normalForm, leftCat, rightEC.getProducedCCGcat(), SUPPORTED_COMBINATORS);
				for (ProducedCCGcat parentCat : result_cats) {
					// get the expressions of the children and type-cast them into LambdaAbstractions for combination
					//LambdaAbstraction rightExp = (LambdaAbstraction)rightEC.getExpression();
					Expression rightExp = rightEC.getExpression();
					Expression parentExp   = null;

					// apply the combinator that was used
					switch (parentCat.getCombinatorUsed()) {
					case FW_APPLY:   // forward application
						parentExp = ((LambdaAbstraction) leftExp).applyTo(rightExp);
						break;
					case BW_APPLY:   // backwards application
						parentExp = ((LambdaAbstraction) rightExp).applyTo(leftExp);
						break;
					case FW_COMPOSE: // generalized forward composition
						if (parentCat.getArityOfCombinatorUsed() == 1) 	 parentExp = ((LambdaAbstraction) leftExp).compose_b1((LambdaAbstraction)rightExp); // b1
						else if (parentCat.getArityOfCombinatorUsed() == 2) parentExp = ((LambdaAbstraction) leftExp).compose_b2((LambdaAbstraction)rightExp); // b2
						break;
					case BW_COMPOSE: // generalized backwards composition
						if (parentCat.getArityOfCombinatorUsed() == 1)      parentExp = ((LambdaAbstraction) rightExp).compose_b1((LambdaAbstraction)leftExp); // b1
						else if (parentCat.getArityOfCombinatorUsed() == 2) parentExp = ((LambdaAbstraction) rightExp).compose_b2((LambdaAbstraction)leftExp); // b2
						break;
					default:
						break;
					}

					// if any of these were  successful, add this new item
					if (parentCat != null && parentExp != null) {
						ChartItem parent = this.ciFactory.internalItem(new SemanticNormalFormEquivalenceClass(parentCat, parentExp), start, end);
						BinaryBackpointer bp = new BinaryBackpointer(leftItem, rightItem);
						bp.setScore(this.scoreBinaryBackpointer(parent, bp));
						parent.addBackpointer(bp);
						binaryParents.add(parent);
					}					
				}	
			}			
		}
		return binaryParents;
	}

	@Override
	public Collection<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> checkForRootItem(
			CKYChart<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> chart) {
		return this.checkForRootItem(chart, null);
	}

	public Collection<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> checkForRootItem(
			CKYChart<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> chart,
			String atomicAnalysisPrefix) {
		Cell<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> topCell 
		= chart.getCell(0, chart.sentence().length()-1);
		ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> root = 
				this.ciFactory.internalItem(this.getRootEC(), 0, topCell.spanEndIndex());

		for (Entry<SemanticNormalFormEquivalenceClass, 
				ChartItem<SemanticExpressionCategory, 
				SemanticNormalFormEquivalenceClass>> entry : topCell.items().mapEntries()) {
			SemanticNormalFormEquivalenceClass ec = entry.getKey();
			ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass> item = entry.getValue();
			CCGcat ccgCat = ec.getProducedCCGcat().getProducedCCGcat();
			if(atomicAnalysisPrefix == null
					|| (ccgCat.isAtomic() && ccgCat.catString().startsWith(atomicAnalysisPrefix))) {
				root.addBackpointer(new UnaryBackpointer(item));
			}
		}		
		// Iff at least one backpointer was found, add root to list of returned items
		ArrayList<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>> rootItems = 
				new ArrayList<ChartItem<SemanticExpressionCategory, SemanticNormalFormEquivalenceClass>>(); // Note: we'll only really make one root item.
		if(!root.backpointers().isEmpty()) {
			rootItems.add(root);
		}
		else if(!topCell.items().isEmpty()
				&& atomicAnalysisPrefix != null) {
			System.err.println("WARNING: Semantic parser currently only returns analyses for full sentences; some successful parses were ignored.");
		}
		return rootItems;
	}

	/**
	 * Gets the parsed expressions. Used for generation.
	 * @return Successfully parsed expressions
	 */
	public Collection<Expression> getParsedExpressions(CKYChart<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> chart) {
		ArrayList<Expression> expressions = new ArrayList<Expression>();
		if(!chart.successfullyParsed()) {
			//System.out.println("WARNING: May be ignoring some expressions, we only report expressions for sentences.");
			return expressions;
		}
		for(Backpointer<SemanticExpressionCategory, 
				SemanticNormalFormEquivalenceClass> bp : chart.root().backpointers()) {
			ChartItem<SemanticExpressionCategory, 
			SemanticNormalFormEquivalenceClass> child = ((UnaryBackpointer) bp).child();
			boolean alreadyAdded = false;
			Expression childExp = child.getEquivalenceClass().getExpression();
			for (Expression exp : expressions) {
				if(childExp.isEquivalentTo(exp)) {
					alreadyAdded = true;
					break;
				}
			}
			if(!alreadyAdded) {
				expressions.add(childExp);
			}
		}
		return expressions;
	}

	protected SemanticNormalFormEquivalenceClass getRootEC() {
		return new SemanticNormalFormEquivalenceClass(
				new ProducedCCGcat(Rule_Type.TYPE_TOP, -1, 
						CCGcat.lexCat("DUMMY", "TOP", "DUMMY", -1)), new Expression());
	}
}
