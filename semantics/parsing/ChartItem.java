package semantics.parsing;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Scanner;

public abstract class ChartItem<C extends Category, E extends EquivalenceClass<C>> {
	
	protected int spanStart;
	
	protected int spanEnd;
	
	protected E equivalenceClass;
	
	protected Backpointer<C, E> viterbi;
	
	public ChartItem(E equivClass, int spanStartIndex, int spanEndIndex) {
		this.spanStart = spanStartIndex;
		this.spanEnd = spanEndIndex;
		this.equivalenceClass = equivClass;
		this.viterbi = null;
	}
	
	public abstract void addBackpointersFromEquivalentItem(ChartItem<C, E> other);
	
	public abstract void addBackpointer(Backpointer<C, E> backpointer);
	
	@Override
	public int hashCode() {
		return this.equivalenceClass.hashCode();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object o) {
		if(o == null || !(o instanceof ChartItem)) {
			return false;
		}
		ChartItem<C, E> oci = (ChartItem<C, E>) o;
		return this.equivalenceClass.equals(oci.equivalenceClass)
				&& this.spanStart == oci.spanStart
				&& this.spanEnd == oci.spanEnd;
	}
	
	public int spanStartIndex() {
		return this.spanStart;
	}
	
	public int spanEndIndex() {
		return this.spanEnd;
	}
	
	public E getEquivalenceClass() {
		return this.equivalenceClass;
	}
	
	public double viterbiScore() {
		if(this.viterbi != null) {
			return this.viterbi.score();
		}
		return Double.NEGATIVE_INFINITY;
	}

	public abstract Collection<Backpointer<C, E>> backpointers();
	
	public Backpointer<C, E> viterbi() {
		return this.viterbi;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(this.spanStart+", ");
		sb.append(this.spanEnd+"] ");
		sb.append(this.equivalenceClass);
		return sb.toString();
	}
	
	protected static final String FOREST_DELIMITER = "###";
	
	public void printForest(PrintWriter pw, int numItems) {
		pw.println(FOREST_DELIMITER);
		pw.println(numItems);
		
	}
	
	public static ChartItem readForest(Scanner sc) {
		String header = sc.nextLine();
		if(!header.equals(FOREST_DELIMITER)) {
			System.err.println("ERROR: "+header+" does not match forest delimter "+FOREST_DELIMITER);
			return null;
		}
		return null;
	}
	
	protected static ChartItem readForestRecurse(Scanner sc) {
		return null;
	}
	
	public abstract Class<? extends ChartItemFactory> getFactory();

	public void setViterbiBackpointer(Backpointer newVitBP) {
		this.viterbi = newVitBP;
	}

}
