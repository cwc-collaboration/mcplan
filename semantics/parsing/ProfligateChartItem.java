package semantics.parsing;

import java.util.ArrayList;
import java.util.Collection;


/**
 * /präfləɡət/ (adjective): recklessly extravagant or wasteful in the use of resources.
 * 
 * A ProfligateChartItem has no bound on the number of backpointers it can maintain.
 * 
 * @author ramusa2
 *
 * @param <C, E>
 */
public class ProfligateChartItem<C extends Category, E extends EquivalenceClass<C>> extends ChartItem<C, E> {

	protected ArrayList<Backpointer<C, E>> backpointers;
	
	public ProfligateChartItem(E equivClass, int spanStartIndex, int spanEndIndex) {
		super(equivClass, spanStartIndex, spanEndIndex);
		this.backpointers = new ArrayList<Backpointer<C, E>>();
	}

	@Override
	public void addBackpointersFromEquivalentItem(ChartItem<C, E> other) {
		for(Backpointer<C, E> bp : ((ProfligateChartItem<C, E>)other).backpointers) {
			this.addBackpointer(bp);
		}
	}

	@Override
	public void addBackpointer(Backpointer<C, E> backpointer) {
		if(this.viterbi == null || backpointer.score() > this.viterbiScore()) {
			this.viterbi = backpointer;
		}
		this.backpointers.add(backpointer);
	}

	@Override
	public Collection<Backpointer<C, E>> backpointers() {
		return this.backpointers;
	}

	@Override
	public Class<? extends ChartItemFactory> getFactory() {
		return ProfligateChartItemFactory.class;
	}

}
