package semantics.parsing;


public abstract class ChartItemFactory<C extends Category, E extends EquivalenceClass<C>>
									implements Loadable<ChartItemFactory<C, E>> {
	
	public LexicalCategoryChartItem<C, E> lexicalCategory(E lexEQ, int index, double score) {
		return new LexicalCategoryChartItem<C, E>(lexEQ, index, score);
	}

	public abstract ChartItem<C, E> internalItem(E ec, int spanStartIndex, int spanEndIndex);

}
