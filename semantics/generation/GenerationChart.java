package semantics.generation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;

import semantics.ccg.CCGcat;
import semantics.ccg.NF;
import semantics.ccg.ProducedCCGcat;
import semantics.ccg.Rule_Type;
import semantics.lambdaFOL.ChartGenItem;
import semantics.lambdaFOL.Expression;
import semantics.lambdaFOL.GenerationTarget;
import semantics.lambdaFOL.LambdaAbstraction;

/**
 * A class representing a generation chart. A chart holds information about the target expression, a set of working edges,
 * and an agenda of edges to be processed. The chart is constructed given a specific GenerationTarget. Edges are instantiated
 * by calling instantiateLexicalEntries() and passing a list of LexicalEntry objects pulled from the lexicon.
 * The chart also runs the basic generation algorithm by calling generate() once 
 * @author nrynchn2
 *
 */
public class GenerationChart {
	private GenerationTarget target;							// target expression
//	private HashSet<ChartEdge> chart;							// set of edges that have been moved to the chart
	private ArrayList<ChartEdge> chart;							// TODO: move back to using HashSet
	private PriorityQueue<ChartEdge> agenda;					// agenda of edges to be processed
//	private HashSet<ChartEdge> completed;						// set of completed edges that match target expression
	private ArrayList<ChartEdge> completed;						// TODO: move back to using HashSet
	private Collection<ChartEdge> nullSem;						// collection of semantically null lexical entries
	
	// known failed CCGcat combinations; TODO: check correct usage of static field
	private static HashMap<ProducedCCGcat, HashSet<ProducedCCGcat>> failedCombinations = new HashMap<ProducedCCGcat, HashSet<ProducedCCGcat>>();  
	
	// combinators to try when combining edges
	private static Rule_Type[] combinatorsToTry = {Rule_Type.FW_APPLY, Rule_Type.BW_APPLY};
	
	/**
	 * Construct a GenerationChart with specified target expression.
	 * @param t Target expression
	 */
	public GenerationChart(GenerationTarget t, Collection<LexicalEntry> s) {
		target    = t;
//		chart     = new HashSet<ChartEdge>();
		chart 	  = new ArrayList<ChartEdge>();
		agenda    = new PriorityQueue<ChartEdge>();
//		completed = new HashSet<ChartEdge>();
		completed = new ArrayList<ChartEdge>();
		initializeSemanticallyNullEdges(s);
	}
	
	/**
	 * Initializes semantically null chart edges.
	 * @param s Collection of semantically null lexical entries
	 */
	void initializeSemanticallyNullEdges(Collection<LexicalEntry> s) {
		nullSem   = new ArrayList<ChartEdge>();
		for (LexicalEntry entry : s) {
			ArrayList<ChartGenItem> items = target.computeInstantiations(entry.getExpression());
			if (items != null) { // computeInstantiations returns null if none were found, rather than an empty list
				for (ChartGenItem item : items) {
					ChartEdge e = new ChartEdge(entry.getWord(), entry.getCategory(), item);
					nullSem.add(e);
					if (Configuration.DEBUG) System.out.println("Initialized semantically null edge "+e);
				}
			}
		}
	}
	
	/**
	 * Generate a sentence given the starting set of lexical entries resulting from lexical lookup.
	 * @param entries Collection of initial lexical entries
	 */
	public void generate(Collection<LexicalEntry> entries) {
		instantiateLexicalEntries(entries);
		generate();
	}
	
	/**
	 * Returns the set of completed edges once generate() has been called for this chart.
	 * @return set of completed realizations
	 */
//	public HashSet<ChartEdge> getRealizations() { return completed; }
	public ArrayList<ChartEdge> getRealizations() { return completed; }
	
	/**
	 * Given a list of lexical entries pulled from the lexicon via lexical lookup, instantiate ChartEdges
	 * using computeInstantiations and add them to the agenda. Also, apply unary type-changing and type-raising
	 * rules to the resulting edges, and add those to the agenda.
	 * @param entries List of lexical entries from lexical lookup
	 */
	public void instantiateLexicalEntries(Collection<LexicalEntry> entries) {
		ArrayList<ChartEdge> edgesToAdd = new ArrayList<ChartEdge>();	// working list of edges to be added
																	    // TODO: probably a better way of implementing this?
		
		// instantiate a new ChartEdge object for each ChartGenItem produced from computeInstantiations for each lexical entry
		for (LexicalEntry entry : entries) {
			ArrayList<ChartGenItem> items = target.computeInstantiations(entry.getExpression());
			if (items != null) { // computeInstantiations returns null if none were found, rather than an empty list
				for (ChartGenItem item : items) {
					ChartEdge edge = new ChartEdge(entry.getWord(), entry.getCategory(), item);
					edgesToAdd.add(edge);
					if (Configuration.DEBUG) System.out.println("Instantiated edge "+edge);
				}
			}
		}
		
		// add new edges to agenda
		if (Configuration.DEBUG) System.out.println("\nAdding lexical edges to agenda ...");
		updateAgenda(edgesToAdd);
		
		// apply unary rules and add new edges to agenda
		if (Configuration.DEBUG) System.out.println("\nApplying unary rules to lexical edges ...");
//		updateAgenda(getUnary(edgesToAdd, Rule_Type.TYPE_CHANGE)); // TODO: uncomment when semantics for type-changing rules are implemented
		updateAgenda(getUnary(edgesToAdd, Rule_Type.FW_TYPERAISE));
		if (Configuration.DEBUG) System.out.println();
		
		if (Configuration.DEBUG) {
			System.out.println("\nAll lexical edges have been instantiated.");
			printAgenda();
			printChart();
			System.out.println("-----");
		}
	}
	
	/**
	 * Performs the chart generation algorithm.
	 */
	public void generate() {
		long startTime = System.currentTimeMillis(); // start time
		if (Configuration.DEBUG) System.out.println("\nStarting the generation algorithm.");
		
		// until the agenda is empty:
		while (!agenda.isEmpty()) {
			// TODO: implement a time limit for generation
			if (System.currentTimeMillis()-startTime >= 15000) {
				if (Configuration.DEBUG) System.out.println("Generation time limit of "+15000+" ms exceeded; quitting generation algorithm.");
				break;
			}
			
			ChartEdge currentEdge = agenda.remove(); // remove first edge from agenda and set as current edge
			if (Configuration.DEBUG) {
				System.out.println("\nCurrent edge removed from agenda: "+currentEdge);
				if (chart.contains(currentEdge)) System.out.println("This edge already exists within the chart; skipping.");
			}
			
			// check whether current edge is equivalent to one already in the chart or fails to meet pruning threshold
			if (!chart.contains(currentEdge)) { // TODO: add a pruning threshold based on score
				for (ChartEdge chartEdge : chart) {			
					// check coverage bit vectors and see that they don't overlap
					if (ChartGenItem.isCoverageDisjoint(chartEdge.getChartGenItem(), currentEdge.getChartGenItem())) {
						if (Configuration.DEBUG) {
							System.out.println("\nCurrent edge: "+currentEdge);
							System.out.println("Chart edge:   "+chartEdge);
//							System.out.println("Disjoint?: "+(ChartGenItem.isCoverageDisjoint(chartEdge.getChartGenItem(), currentEdge.getChartGenItem()) ? "YES" : "NO"));
						}
						
						// TODO: implement some way of checking index bit vectors for intersection (i.e., the list of semantic variables available...)
						// TODO: implement checking of incomplete chunks, if we want to for efficiency
						
						// combine current edge with chart edge using all available binary combinatory rules
						ArrayList<ChartEdge> edgesToAdd = combineChartEdges(chartEdge, currentEdge);
						
						// add any resulting new edges to the agenda, updating reference to best complete edge so far (if any); TODO: incorporate scoring for 1-best
						updateAgenda(edgesToAdd);
					}
				}
				
				// apply unary combinatory rules to current edge, adding resulting new edges to agenda
				if (Configuration.DEBUG) System.out.println("\nApplying unary rules to current edge ...");
//				updateAgenda(getUnary(currentEdge, Rule_Type.TYPE_CHANGE)); // TODO: uncomment when semantics for type-changing rules are implemented
				updateAgenda(getUnary(currentEdge, Rule_Type.FW_TYPERAISE));		
				
				// combine current edge with edges for all semantically null lexical items
				// TODO: implement syntactic features or edge pruning to avoid infinite loop here
				// FIXME: make sure any semantically null modifiers have different features!!! or else we're hosed
				updateAgenda(combineWithSemanticallyNullEdges(currentEdge));
				
				// add the current edge to the chart
				if (!chart.contains(currentEdge)) {
					boolean added = chart.add(currentEdge);
					// TODO: if number of edges with same category exceeds the pruning threshold, prune the lowest scoring edge in this group from the chart

					if (Configuration.DEBUG) {
						if (added) System.out.println("\nCurrent edge added to chart: "+currentEdge);
						printAgenda();
						printChart();
					}
				}				
			}
			
			if (Configuration.DEBUG) System.out.println("-----");

		}
		
		long endTime = System.currentTimeMillis();
		if (Configuration.DEBUG) System.out.println("\n\nGeneration finished! Time taken: "+(endTime-startTime)+"ms");
	}
	
	/**
	 * Attempts to combine two chart edges.
	 * @param edge1 First child edge
	 * @param edge2 Second child edge
	 * @return List of resulting edges
	 */
	ArrayList<ChartEdge> combineChartEdges(ChartEdge edge1, ChartEdge edge2) {
		ArrayList<ChartEdge> resultingEdges = new ArrayList<ChartEdge>();
		resultingEdges.addAll(combineChartEdgeHelper(edge1, edge2)); // edge1 is left edge, edge2 is right edge
		resultingEdges.addAll(combineChartEdgeHelper(edge2, edge1)); // edge2 is left edge, edge1 is right edge
		return resultingEdges;
	}
	
	/**
	 * Attempts to combine two chart edges given the left-right ordering of the edges.
	 * @param left_edge Left child edge
	 * @param right_edge Right child edge
	 * @return List of resulting edges
	 */
	ArrayList<ChartEdge> combineChartEdgeHelper(ChartEdge left_edge, ChartEdge right_edge) {
		ArrayList<ChartEdge> resultingEdges = new ArrayList<ChartEdge>();
		ProducedCCGcat left_cat  = left_edge.getProducedCCGcat();	// left category
		ProducedCCGcat right_cat = right_edge.getProducedCCGcat();	// right category
		
		if (Configuration.DEBUG) {
			System.out.print("Left cat: "+left_edge.getSurfaceString()+" "+left_cat.catString()+" "+left_edge.getChartGenItem().expression());
			System.out.println("\t\tRight cat: "+right_edge.getSurfaceString()+" "+right_cat.catString()+" "+right_edge.getChartGenItem().expression());
		}
		
		// initialize list of right-hand categories for given left-hand category that failed to combine in any way, if it doesn't already exist
//		if (!failedCombinations.containsKey(left_cat)) failedCombinations.put(left_cat, new HashSet<ProducedCCGcat>());
//		if (Configuration.DEBUG && failedCombinations.get(left_cat).contains(right_cat)) System.out.println("This combination has been known to fail; skipping.");
		
		// attempt to combine edges if this combination of categories hasn't been known to fail
//		if (!failedCombinations.get(left_cat).contains(right_cat)) {
			// attempt to combine the CCG categories; if no resulting categories are returned, add this combination to the list of known failures
			ArrayList<ProducedCCGcat> result_cats = CCGcat.combineCheckNormalForm(Configuration.NF, left_cat, right_cat, Arrays.asList(combinatorsToTry));
			if (result_cats == null || result_cats.isEmpty()) {
//				failedCombinations.get(left_cat).add(right_cat);
				if (Configuration.DEBUG) System.out.println("This combination failed; remembering.");
			}
			
			// for every result category, attempt to combine the corresponding semantic expressions
			for (ProducedCCGcat result_cat : result_cats) {
				if (Configuration.DEBUG) System.out.println("Resulting cat from "+result_cat.getCombinatorUsed().toString()+": "+result_cat.catString());
				LambdaAbstraction left_exp  = (LambdaAbstraction)left_edge.getChartGenItem().expression();	// left expression
				LambdaAbstraction right_exp = (LambdaAbstraction)right_edge.getChartGenItem().expression(); // right expression
				Expression result_exp   = null;
				
				// combine the expressions according to the rule used to combine the categories
				switch (result_cat.getCombinatorUsed()) {
				case FW_APPLY:   // forward application
					result_exp = left_exp.applyTo(right_exp);
					break;
				case BW_APPLY:   // backward application
					result_exp = right_exp.applyTo(left_exp);
					break;
				case FW_COMPOSE: // generalized forward composition
					if (result_cat.getArityOfCombinatorUsed() == 1)      {
						result_exp = left_exp.compose_b1(right_exp); // b1
						if (isCrossingComposition(left_cat, right_cat, result_cat.getArityOfCombinatorUsed())) {
							if (Configuration.DEBUG) System.out.println("Forward crossing composition: "+left_cat.catString()+" "+left_edge.getSurfaceString()+", "+right_cat.catString()+" "+right_edge.getSurfaceString());
							if (right_cat.catString().equals("NP\\NP")) result_exp = null;
						}
					}
					else if (result_cat.getArityOfCombinatorUsed() == 2) result_exp = left_exp.compose_b2(right_exp); // b2
					break;
				case BW_COMPOSE: // generalized backward composition
					// if backwards crossing, restrict primary functors to adjunct categories
					if (isCrossingComposition(left_cat, right_cat, result_cat.getArityOfCombinatorUsed()) && right_cat.getProducedCCGcat().isAdjunctCat()) { 
						if (result_cat.getArityOfCombinatorUsed() == 1)      result_exp = right_exp.compose_b1(left_exp); // b1
						else if (result_cat.getArityOfCombinatorUsed() == 2) result_exp = right_exp.compose_b2(left_exp); // b2
					}
					break;
				default:
					break;
				}
				
				if (result_exp != null) {
					// if a result expression was successfully created, compute its instantiations in the target expression and expected joint coverage
					ArrayList<ChartGenItem> result_items = target.computeInstantiations(result_exp);
					boolean[] jointCoverage = ChartGenItem.jointCoverage(left_edge.getChartGenItem(), right_edge.getChartGenItem());
					if (Configuration.DEBUG) System.out.println("Resulting expression: "+result_exp+"\t\t# instantiations: "+(result_items != null ? result_items.size() : 0));

					if (result_items != null) { // computeInstantiations returns null if none were found, rather than an empty list
						for (int i = 0; i < result_items.size(); i++) {
							ChartGenItem result_item = result_items.get(i);
							if (Configuration.DEBUG) System.out.println("Does joint coverage of instantiation "+(i+1)+" match?: "+(result_item.coverageEquals(jointCoverage) ? "YES" : "NO"));
							
							// if the coverage of the resulting edge equals the joint coverage expected, add this new edge to list of resulting edges
							if (result_item.coverageEquals(jointCoverage)) {
								ChartEdge e = new ChartEdge(left_edge.getSurfaceString()+" "+right_edge.getSurfaceString(), result_cat, result_item, left_edge, right_edge);
								resultingEdges.add(e);
								if (Configuration.DEBUG) System.out.println("Resulting edge: "+e);
							}
						}
					}
				}
			}
//		}
		if (Configuration.DEBUG) System.out.println();
		return resultingEdges;
	}
	
	/**
	 * Checks if two categories are composed using crossing composition.
	 * @param left Left category
	 * @param right Right category
	 * @param arity Arity of composition
	 * @return true if crossing composition; false otherwise
	 */
	boolean isCrossingComposition(ProducedCCGcat left, ProducedCCGcat right, int arity) {
		if (arity == 1) return left.getProducedCCGcat().argDir() != right.getProducedCCGcat().argDir(); // b1
		return false; // TODO: how to check for crossing in B2, B3?
	}
	
	/**
	 * Attempts to combine the given edge with all semantically null edges in the lexicon.
	 * @param edge The edge to be combined
	 * @return List of resulting edges
	 */
	ArrayList<ChartEdge> combineWithSemanticallyNullEdges(ChartEdge edge) {
		ArrayList<ChartEdge> resultingEdges = new ArrayList<ChartEdge>();	
		for (ChartEdge e : nullSem)
			// attempt to combine edges if the coverage vectors are disjoint
			if (ChartGenItem.isCoverageDisjoint(edge.getChartGenItem(), e.getChartGenItem()))
				resultingEdges.addAll(combineChartEdges(edge, e));
		return resultingEdges;
	}
	
	/**
	 * Adds the given list of edges to the agenda, and marks an edge as completed if its expression matches the target expression.
	 * @param edgesToAdd List of edges to add
	 */
	void updateAgenda(ArrayList<ChartEdge> edgesToAdd) {
		agenda.addAll(edgesToAdd);
		
		// check equivalence of each edge to the target expression and save if completed
		for (ChartEdge e : edgesToAdd) {
			if (Configuration.DEBUG) System.out.println("Adding edge to agenda: "+e);
			if (target.isEquivalentToTarget(e.getChartGenItem().expression()) && (e.getProducedCCGcat().catString().equals("S") || e.getProducedCCGcat().catString().startsWith("N"))) {
				if (!completed.contains(e)) completed.add(e);
				if (Configuration.DEBUG) System.out.println("\t--- Found completed edge!");
			}
		}
	}
	
	/**
	 * Attempts to apply unary rules of the given type to the given edge.
	 * @param edge The edge to be combined
	 * @param type The unary rule type to be used
	 * @return List of resulting edges
	 */
	ArrayList<ChartEdge> getUnary(ChartEdge edge, Rule_Type type) {
		ArrayList<ChartEdge> resultingEdges = new ArrayList<ChartEdge>();	// working list of edges to be added
																		// TODO: probably a better way of implementing this?
		
		// TYPE_CHANGE: TODO: currently only implements N -> NP
		if (type == Rule_Type.TYPE_CHANGE && NF.unaryNF(Configuration.NF, edge.getProducedCCGcat().getCombinatorUsed(), type)) {
			if (edge.getProducedCCGcat().catString().equals("N")) {
				// TODO: produce semantics for this type-changing rule
				// the new edge has same surface string, type-changed CCGcat, and same ChartGenItem object as the original edge
				ChartEdge e = new ChartEdge(edge.getSurfaceString(), 
						CCGcat.getTypeChangedWithNormalFormInfo(edge.getProducedCCGcat(), "NP"), edge.getChartGenItem(), edge);
				resultingEdges.add(e);
				if (Configuration.DEBUG) System.out.println(e.getProducedCCGcat().getCombinatorUsed()+" success: "+edge+" -> "+e);
			}
		}
		
		// FW_TYPERAISE: TODO: currently only implements NP -> S/(S\NP)
		else if (type == Rule_Type.FW_TYPERAISE) {
			// attempt to type-raise the CCGcat
			ProducedCCGcat result_cat = null;
			switch (edge.getProducedCCGcat().catString()) {
			case "NP":
				result_cat = CCGcat.getTypeRaisedCatWithNormalFormInfo(
						edge.getProducedCCGcat().getProducedCCGcat(), "S/(S\\NP)");
				break;
			default:
				break;
			}

			// attempt to type-raise the Expression
			Expression result_exp = edge.getChartGenItem().expression().typeRaise();
			if (result_cat != null && result_exp != null) {
				// add a chart edge with the same surface string and coverage, using the type-raised category and expression
				ChartEdge e = new ChartEdge(edge.getSurfaceString(), result_cat, new ChartGenItem(target, edge.getChartGenItem().coverage(), result_exp), edge);
				resultingEdges.add(e);
				if (Configuration.DEBUG) System.out.println(result_cat.getCombinatorUsed().toString()+" success: "+edge+" type-raised to "+e);
			}
		}
		
		return resultingEdges;
	}
	
	/**
	 * Apply unary rules to all edges in a given list of edges.
	 * @param edges List of edges to be used
	 * @param type Type of rule to be used (e.g., TYPE_CHANGE or FW_TYPERAISE)
	 * @return list of edges resulting from unary rule applied
	 */
	ArrayList<ChartEdge> getUnary(ArrayList<ChartEdge> edges, Rule_Type type) {
		ArrayList<ChartEdge> resultingEdges = new ArrayList<ChartEdge>(); 	// working list of edges to be added
	    																// TODO: probably a better way of implementing this?
		
		for (ChartEdge edge : edges) resultingEdges.addAll(getUnary(edge,type));
		return resultingEdges;
	}
	
	/**
	 * Prints the current contents of the agenda for debugging.
	 */
	void printAgenda() {
		System.out.println("\nPrinting agenda's current contents: ");
		for (ChartEdge e : agenda) System.out.println(e);
		if (agenda.isEmpty()) System.out.println("<empty>");
		System.out.println();
	}
	
	/**
	 * Prints the current contents of the chart for debugging.
	 */
	void printChart() {
		System.out.println("\nPrinting chart's current contents: ");
		for (ChartEdge e : chart) System.out.println(e);
		if (chart.isEmpty()) System.out.println("<empty>");
		System.out.println();
	}
}
