package semantics.generation;

import java.util.*;

import semantics.lambdaFOL.*;
import semantics.lambdaFOL.Expression.TerminalType;

/**
 * A chart generator. Initialized with the semantic lexicon to be used and preprocesses the lexicon for lexical lookup.
 * The chart generator object handles accessing lexical entries from the lexicon for lookup given a GenerationTarget by 
 * calling lexicalLookup() and passing in the target. This keeps GenerationChart objects independent of the specific lexicon
 * used in the instance.
 * @author nrynchn2
 *
 */
public class ChartGenerator {
	protected static SemLexicon lexicon;								// semantic lexicon
	
	/**
	 * Fields used to preprocess the lexicon for lexical lookup.
	 */
	private static HashMap<String, Integer> predicateCounts;		// counts of predicate/constant instances in lexicon
	private static ArrayList<String> sortedPredicates;				// list of predicate/constants in sorted increasing order of frequency
	private static HashMap<String, ArrayList<LexicalEntry>> lookup;	// string to lexical entry mapping for lexical lookup
	
	/**
	 * Constructor
	 * @param l semantic lexicon
	 */
	public ChartGenerator(SemLexicon l) {
		lexicon = l;
		preprocessLexicon();
	}
	
	/**
	 * Preprocesses the lexicon: counts and sorts predicates, and initializes the lookup table for lexical lookup.
	 */
	public static void preprocessLexicon() {
		countAndSortPredicates();
		initializeLookupTable();
	}
	
	/**
	 * Counts and sorts the predicates.
	 */
	public static void countAndSortPredicates() {
		predicateCounts = new HashMap<String, Integer>();
		
		// TODO: do we include quantifiers? Then we might look up determiners, etc. using forall, unique, and exists
		// for each entry in the lexicon, count instances of predicates and constants contained in the expression
		for (String word : lexicon.getWordsInLexicon()) {
			for (String category : lexicon.getAllEntries(word).keySet()) {
				for (Expression expression : lexicon.getEntry(word, category)) {
					LambdaAbstraction exp   = (LambdaAbstraction) expression;
					ArrayList<String> yield = exp.yield();
					ArrayList<TerminalType> yieldTypes = exp.yieldTypes();
					
					for (int i = 0; i < yieldTypes.size(); i++) {
						if (yieldTypes.get(i).equals(TerminalType.CONSTANT) || yieldTypes.get(i).equals(TerminalType.PREDICATE) || yieldTypes.get(i).equals(TerminalType.QUANTIFIER)) {
							String predicate = yield.get(i);
							if (!predicateCounts.containsKey(predicate)) predicateCounts.put(predicate, 0);
							predicateCounts.put(predicate, predicateCounts.get(predicate)+1);
						}
					}
				}
			}
		}
		
		// sort predicates by their counts accumulated over the lexicon in ascending order
		sortedPredicates = new ArrayList<String>(predicateCounts.keySet());
		Collections.sort(sortedPredicates, new Comparator<String>() {
			@Override
			public int compare(String x, String y) { return predicateCounts.get(x) - predicateCounts.get(y); }
		});
		
		if (Configuration.DEBUG) {
			System.out.println("countAndSortPredicates: Sorted predicate list");
			for (String word : sortedPredicates) System.out.println(word + ": " + predicateCounts.get(word));
			System.out.println();
		}
	}
	
	/**
	 * Initializes the lexical lookup table.
	 */
	public static void initializeLookupTable() {
		lookup = new HashMap<String, ArrayList<LexicalEntry>>();
		
		// TODO: a better solution for lexical lookup?
		// for each entry in the lexicon, associate its lookup key with the predicate/constant contained within the expression  
		// that is seen least frequently in the lexicon
		for (String word : lexicon.getWordsInLexicon()) {
			for (String category : lexicon.getAllEntries(word).keySet()) {
				for (Expression expression : lexicon.getEntry(word, category)) {
					LexicalEntry entry = new LexicalEntry(word, category, expression);
					ArrayList<String> yield = expression.yield();
					ArrayList<TerminalType> yieldTypes = expression.yieldTypes();
					
					String principalPredicate   = null;
					int principalPredicateIndex = Integer.MAX_VALUE;
					for (int i = 0; i < yield.size(); i++) {
						if (yieldTypes.get(i).equals(TerminalType.CONSTANT) || yieldTypes.get(i).equals(TerminalType.PREDICATE) || yieldTypes.get(i).equals(TerminalType.QUANTIFIER)) {
							String predicate = yield.get(i);
							int predicateIndex = sortedPredicates.indexOf(predicate);
							if (predicateIndex > -1 && predicateIndex < principalPredicateIndex) {
								principalPredicateIndex = predicateIndex;
								principalPredicate = predicate;
							}
						}
					}
					
					if (!lookup.containsKey(principalPredicate)) lookup.put(principalPredicate, new ArrayList<LexicalEntry>());
					lookup.get(principalPredicate).add(entry);
				}
			}
		}
		
		if (Configuration.DEBUG) {
			System.out.println("initializeLookupTable: Lexical lookup table");
			for (String key : lookup.keySet()) System.out.println(key + ": " + lookup.get(key));
			System.out.println();
		}
	}
	
	/**
	 * Performs lexical lookup given a generation target.
	 * @param target Generation target
	 * @return Collection of lexical entries found to have predicates in the generation target
	 */
	public static Collection<LexicalEntry> lexicalLookup(GenerationTarget target) {
		HashSet<LexicalEntry> lexicalEntries = new HashSet<LexicalEntry>();
		
		for (int i = 0; i < target.getTargetYieldLength(); i++) {
			String predicate = target.getTargetYield().get(i);
			if (lookup.containsKey(predicate)) lexicalEntries.addAll(lookup.get(predicate));
		}
		
		if (Configuration.DEBUG) {
			System.out.println("\nLexical lookup for generation target: "+target.getTargetExpression());
			for (LexicalEntry e : lexicalEntries) System.out.println(e);
			System.out.println();
		}
		
		return lexicalEntries;
	}
	
	public static Collection<LexicalEntry> getSemanticallyNullLexicalItems() {
		if (!lookup.containsKey(null)) lookup.put(null, new ArrayList<LexicalEntry>());
		return lookup.get(null);
	}
	
	public ArrayList<ChartEdge> generate(Expression expression) {
		GenerationTarget target = new GenerationTarget(expression);
		
		if (Configuration.DEBUG) System.out.println("Initializing the GenerationChart.");
		GenerationChart  chart  = new GenerationChart(target, getSemanticallyNullLexicalItems());
		Collection<LexicalEntry> lexicalEntries = lexicalLookup(target);
		chart.generate(lexicalEntries);
		return chart.getRealizations();
	}
}
