package semantics.generation;

public class ChartEdgeCombination {
	private ChartEdge left;
	private ChartEdge right;
	private ChartEdge result;

	public ChartEdgeCombination(ChartEdge l, ChartEdge r, ChartEdge res) {
		left   = l;
		right  = r;
		result = res;
	}
	
	public ChartEdgeCombination(ChartEdge l, ChartEdge res) {
		left   = l;
		right  = null;
		result = res;
	}
	
	public ChartEdgeCombination(ChartEdge l) {
		left   = l;
		right  = null;
		result = null;
	}
	
	public String toString() {
		if (right != null)  return "Left:  "+left.toStringNoCoverage()+"\t\tRight: "+right.toStringNoCoverage()+"\t\t"+result.getProducedCCGcat().getCombinatorUsed()+"\t\tResult: "+result.toStringNoCoverage();
		if (result != null) return "Unary: "+left.toStringNoCoverage()+"\t\t"+result.getProducedCCGcat().getCombinatorUsed()+"\t\tResult: "+result.toStringNoCoverage();
		return "Lex:   "+left.toStringNoCoverage();
	}
}
