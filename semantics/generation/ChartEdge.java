package semantics.generation;

import java.util.ArrayList;

import semantics.ccg.CCGcat;
import semantics.ccg.ProducedCCGcat;
import semantics.ccg.Rule_Type;
import semantics.lambdaFOL.ChartGenItem;

public class ChartEdge implements Comparable<ChartEdge> {
	private static int	   dummyScore = 0;
	private String 		   surface;
	private ProducedCCGcat category;
	private ChartGenItem   semantics;
	private int score;
	private ArrayList<ChartEdgeCombination> history;
	
	/**
	 * Constructor for lexical entries (i.e., used at instantiation of the chart).
	 * @param s Word in lexicon
	 * @param c String representation of CCG category
	 * @param g ChartGenItem returned from computeInstantiations()
	 */
	public ChartEdge(String s, String c, ChartGenItem g) {
		surface   = s;
		category  = new ProducedCCGcat(Rule_Type.LEXICAL_CATEGORY, -1, CCGcat.lexCat(s, c, null, -1));
		semantics = g;
		score     = dummyScore;
		dummyScore += 1;
		history   = new ArrayList<ChartEdgeCombination>();
		history.add(new ChartEdgeCombination(this));
	}
	
	public ChartEdge(String s, ProducedCCGcat c, ChartGenItem g, ChartEdge u) {
		surface   = s;
		category  = c;
		semantics = g;
		score     = dummyScore;
		dummyScore += 1;
		history = new ArrayList<ChartEdgeCombination>();
		history.addAll(u.getHistory());
		history.add(new ChartEdgeCombination(u, this));
	}
	
	public ChartEdge(String s, ProducedCCGcat c, ChartGenItem g, ChartEdge l, ChartEdge r) {
		surface   = s;
		category  = c;
		semantics = g;
		score     = dummyScore;
		dummyScore += 1;
		history = new ArrayList<ChartEdgeCombination>();
		history.addAll(l.getHistory());
		history.addAll(r.getHistory());
		history.add(new ChartEdgeCombination(l, r, this));
	}
	
	public String		  getSurfaceString()  { return surface;   }
	public ProducedCCGcat getProducedCCGcat() { return category;  }
	public ChartGenItem   getChartGenItem()   { return semantics; }
	public int 			  getScore() 		  { return score;     }
	public ArrayList<ChartEdgeCombination> getHistory() { return history; }

	public String toString() {
		return surface + " " + category.catString() + " " + score + " " + semantics;
//		return surface + " " + category.catString() + " " + " " + semantics;

	}
	
	public String toStringNoCoverage() {
		return surface + " " + category.catString() + " " + score + " " + semantics.expression();
	}
	
	public void printHistory() {
		System.out.println("Trace:");
		for (ChartEdgeCombination c : history) System.out.println("\t"+c);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		ChartEdge other = (ChartEdge) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (semantics == null) {
			if (other.semantics != null)
				return false;
		} else if (!semantics.equals(other.semantics))
			return false;
		if (surface == null) {
			if (other.surface != null)
				return false;
		} else if (!surface.equals(other.surface))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result
				+ ((semantics == null) ? 0 : semantics.hashCode());
		result = prime * result + ((surface == null) ? 0 : surface.hashCode());
		return result;
	}

	@Override
	public int compareTo(ChartEdge o) {
		return this.score - o.score; // TODO: check that this works
	}
}
