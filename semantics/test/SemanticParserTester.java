package semantics.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import semantics.lambdaFOL.Expression;
import semantics.parsing.SemanticCKYParser;
import semantics.parsing.Sentence;


public class SemanticParserTester {

	public static void main(String[] args) {
		String lexiconFile = "data/semantics/testlexicon.txt";
		SemanticCKYParser parser = new SemanticCKYParser(lexiconFile);
		String senFile = "data/semantics/testsuite.txt";
		Collection<Sentence> sentences = readSentences(senFile);
		for(Sentence sen : sentences) {
			Collection<Expression> expressions = parser.parseSentence(sen);
			System.out.println("Parsed sentence: "+sen);
			if(expressions.isEmpty()) {
				System.out.println("  No expressions.");
			}
			else {
				System.out.println("  Expressions:");
				for(Expression exp : expressions) {
					System.out.println("    "+exp);
				}
			}
			System.out.println("---");
		}
	}


	private static Collection<Sentence> readSentences(String testFile) {
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		try {
			Scanner sc = new Scanner(new File(testFile));
			while(sc.hasNextLine()) {
				String line = sc.nextLine();
				if(!line.isEmpty()) {
					sentences.add(new Sentence(line));
				}
			}
			sc.close();
		}
		catch(Exception e) {
			System.err.println("Unable to read sentences from: "+testFile);
			e.printStackTrace();
		}
		return sentences;
	}
}
