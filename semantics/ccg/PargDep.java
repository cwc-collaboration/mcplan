package semantics.ccg;

public class PargDep implements Comparable<PargDep> {
	
	protected String category;
	
	protected int arg;
	
	protected int head;
	
	protected int slot;
	
	protected boolean bounded;
	
	protected boolean extracted;
	
	protected volatile int hashCode;
	
	public PargDep(int arg, int head, String category, int slot) {
		this(arg, head, category, slot, false, false);
	}
	
	public PargDep(int arg, int head, String category, int slot, 
			boolean extracted, boolean bounded) {
		this.arg = arg;
		this.head = head;
		this.category = category;
		this.slot = slot;
		this.bounded = bounded;
		this.extracted = extracted;
	}
	
	public int getArg() {
		return this.arg;
	}
	
	public int getHead() {
		return this.head;
	}
	
	public String getCategory() {
		return this.category;
	}
	
	public int getArgSlot() {
		return this.slot;
	}
	
	public boolean isBounded() {
		return this.bounded;
	}
	
	public boolean isExtracted() {
		return this.extracted;
	}
	
	public boolean matchesUnlabeledUndirected(PargDep o) {
		return (this.arg == o.arg && this.head == o.head)
				|| (this.arg == o.head && this.head == o.arg);
	}

	public boolean matchesLabeledDirected(PargDep o) {
		return this.arg == o.arg 
				&& this.head == o.head
				&& this.category != null && this.category.equals(o.category)
				&& this.slot == o.slot;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.head);
		sb.append("\t");
		sb.append(this.arg);
		sb.append("\t");
		sb.append(this.category);
		sb.append("\t");
		sb.append(this.slot);
		return sb.toString();
	}

	@Override
	public int compareTo(PargDep o) {
		return (int) Math.signum((this.arg - o.arg)*10000 + (this.head - o.head));
	}
	
	public int hashCode() {
		int result = this.hashCode;
		if(result == 0) {
			result = 17;
			result = 31 * result + arg;
			result = 31 * result + head;
			this.hashCode = result;
		}
		return result;
	}
	
	public boolean equals(Object o) {
		if(!(o instanceof PargDep)) {
			return false;
		}
		PargDep d = (PargDep) o ;
		return this == d
				|| (this.arg == d.arg
					&& this.head == d.head
					&& this.slot == d.slot
					&& this.category != null && this.category.equals(d.category)
					);
	}
}
