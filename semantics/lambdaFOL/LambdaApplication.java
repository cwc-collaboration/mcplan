package semantics.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class LambdaApplication extends Expression {
	protected final Expression functor;
	protected final Expression argument;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	public LambdaApplication(Expression f, Expression a){
		functor  = f;
		argument = a;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	public Expression getFunctor(){
		return functor;
	}
	public Expression getArgument(){
		return argument;
	}
	public String toString(){
		return "(" + functor + " " + argument + ")";
	}
	
	public Expression replace(Variable var, Expression e){
		//System.out.println("Replacing the variable " + var + " with " + e + " in " + this);
		Expression f2 = functor.replace(var, e);
		//System.out.println(" - Functor: from " + this.functor + " to " + f2);
		Expression a2 = argument.replace(var, e);
		//System.out.println(" - Argument: from " + this.argument + " to " + a2);
		if (f2 instanceof LambdaAbstraction){
			return ((LambdaAbstraction) f2).applyTo(a2);
		}
		else return new LambdaApplication(f2, a2);
	}
	
	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat) {
	 if (e != null && e instanceof LambdaApplication){
		 LambdaApplication f = (LambdaApplication)e;
		 if (this.functor.isEquivalentModuloVarRenaming(f.functor, varEquivalenceMapThis, varEquivalenceMapThat)
				 && this.argument.isEquivalentModuloVarRenaming(f.argument, varEquivalenceMapThis, varEquivalenceMapThat)){
			 return true;
		 }
		 else return false;
	 }
	 return false;
	}
	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		 ArrayList<String> yield = new ArrayList<String>();
		 yield.addAll(functor.yield());
		 yield.addAll(argument.yield());
		 return yield;	 
		}
	
	protected ArrayList<TerminalType> initYieldTypes(){
		 ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		 yieldTypes.addAll(functor.yieldTypes());
		 yieldTypes.addAll(argument.yieldTypes());
		 return yieldTypes;	 
		}
	
	public HashSet<Variable> getAllVariables() {
		HashSet<Variable> varSet = new HashSet<Variable>();
		HashSet<Variable> functorVars = functor.getAllVariables();
		if (functorVars != null){
			varSet.addAll(functorVars);
		}
		HashSet<Variable> argumentVars = argument.getAllVariables();
		if (argumentVars != null){
			varSet.addAll(argumentVars);
		}
		return varSet;		
	}
	
	protected Expression renameVar(Variable v, Variable v1) {
		Expression functor1 = functor.renameVar(v, v1);
		Expression arg1 = argument.renameVar(v,v1);
		return new LambdaApplication(functor1, arg1);
	}
	
	public Expression flattenNestedFormulas(){
		Expression functor1 = functor.flattenNestedFormulas();
		Expression arg1 = argument.flattenNestedFormulas();
		return new LambdaApplication(functor1, arg1);
	}
}
