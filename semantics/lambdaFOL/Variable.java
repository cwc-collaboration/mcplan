package semantics.lambdaFOL;

import semantics.lambdaFOL.Expression.VarType;

public interface Variable {
	enum Status {FREE, QUANTIFIED, LAMBDABOUND};
	
	public Status status();
	public String name();
	public VarType type();
	public Variable getNewVariable();
}
