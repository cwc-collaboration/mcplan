package semantics.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class NegatedFormula extends Formula {

	protected final Expression body;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	public NegatedFormula(Expression f){
		body = f;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}

	public String toString(){
		if (body instanceof ComplexFormula)
			return new String(NEG + "(" + body.toString() + ")");
		else
			return new String(NEG + body.toString());
	}

	public int hashCode(){
		return -1 * body.hashCode();
	}
	
	public boolean equals(Object o){
		if (o != null && o instanceof NegatedFormula){
			return body.equals(((NegatedFormula)o).body);
		}
		else return false;
	}
	
	
	public NegatedFormula replace(Variable var, Expression e){
		return new NegatedFormula((Formula)body.replace(var,e));
	}
	
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat){
		if (e != null && e instanceof NegatedFormula){
			NegatedFormula f = (NegatedFormula)e;
			return this.body.isEquivalentModuloVarRenaming(f.body, varEquivalenceMapThis, varEquivalenceMapThat);
		}
		else return false;
	}
	
	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(NEG);
		yield.addAll(body.yield());
		return yield;
	}

	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.add(TerminalType.NEGATION);
		yieldTypes.addAll(body.yieldTypes());
		return yieldTypes;
	}

	public HashSet<Variable> getAllVariables() {
		return body.getAllVariables();
	}

	protected Expression renameVar(Variable v, Variable v1) {
		Expression b1 = body.renameVar(v,v1);
		return new NegatedFormula(b1);
	}

	public Expression flattenNestedFormulas(){
		Expression b1 = body.flattenNestedFormulas();
		return new NegatedFormula(b1);
	}
	
	
}
