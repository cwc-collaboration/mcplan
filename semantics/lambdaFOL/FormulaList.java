/**
 * 
 */
package semantics.lambdaFOL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author juliahmr
 *
 */
public class FormulaList extends Formula {
	final protected Connective con;
	ArrayList<Expression> formulas;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;

	public static FormulaList createFormulaList(Connective c, ArrayList<Expression> f){
		if (c == Connective.AND || c == Connective.OR){
			ArrayList<Expression> fSorted = sortExpressionList(f);
			return new FormulaList(c,fSorted);
		}
		else return null;
	}

	protected FormulaList(Connective c, ArrayList<Expression> f){
		con = c;
		ArrayList<Expression> fSorted = sortExpressionList(f);
		formulas = fSorted;
		yield = initYield();
		yieldTypes = initYieldTypes();
		//formulas = f;
	}

	public Connective connective(){
		return con;
	}
	/**
	 * @param elist
	 * @return a list of expressions in which all atomic formulas appear first (sorted by their predicates), 
	 * followed by all complex formulas, followed by all quantified formulas, followed by all lambda abstractions, followed by all lambda applications, followed by all formula lists.
	 */
	protected static ArrayList<Expression> sortExpressionList(ArrayList<Expression> elist) {//TODO -- implement me
		ArrayList<Expression> sortedList = new ArrayList<Expression>();
		ArrayList<AtomicFormula> atomicFormulaList = new ArrayList<AtomicFormula>();		
		ArrayList<ComplexFormula> complexFormulaList = new ArrayList<ComplexFormula>();
		ArrayList<FormulaList> formulaListList = new ArrayList<FormulaList>();
		ArrayList<QuantifiedFormula> quantifiedFormulaList = new ArrayList<QuantifiedFormula>();
		ArrayList<LambdaAbstraction> lambdaAbstractionList = new ArrayList<LambdaAbstraction>();
		ArrayList<LambdaApplication> lambdaApplicationList = new ArrayList<LambdaApplication>();
		ArrayList<FormulaVariable> formulaVarList = new ArrayList<FormulaVariable>();

		ArrayList<Constant> constantList = new ArrayList<Constant>();
		ArrayList<TermVariable> termVarList = new ArrayList<TermVariable>();

		for (Expression e: elist){
			if (e instanceof AtomicFormula){
				atomicFormulaList.add((AtomicFormula) e);
			}
			else if (e instanceof ComplexFormula){
				complexFormulaList.add((ComplexFormula) e);
			}
			else if (e instanceof FormulaList){
				formulaListList.add((FormulaList) e);
			}
			else if (e instanceof QuantifiedFormula){
				quantifiedFormulaList.add((QuantifiedFormula) e);
			}
			else if (e instanceof LambdaAbstraction){
				lambdaAbstractionList.add((LambdaAbstraction) e);
			}
			else if (e instanceof LambdaApplication){
				lambdaApplicationList.add((LambdaApplication) e);
			}
			else if (e instanceof FormulaVariable){
				formulaVarList.add((FormulaVariable)e);
			}	
			else if (e instanceof Constant){
				constantList.add((Constant)e);
			}
			else if (e instanceof TermVariable){
				termVarList.add((TermVariable)e);
			}

		}
		Collections.sort(atomicFormulaList);// NB: atomic formulas are the only ones that can be sorted (just by their predicate...which is wrong);
		sortedList.addAll(atomicFormulaList);
		sortedList.addAll(complexFormulaList);
		sortedList.addAll(formulaListList);
		sortedList.addAll(quantifiedFormulaList);
		sortedList.addAll(lambdaAbstractionList);
		sortedList.addAll(lambdaApplicationList);
		sortedList.addAll(formulaVarList);
		sortedList.addAll(constantList);
		sortedList.addAll(termVarList);
		return sortedList;
	}

	public boolean isDisjunction(){
		if (con == Connective.OR)
			return true;
		else return false;
	}

	public boolean isConjunction(){
		if (con == Connective.AND)
			return true;
		else return false;
	}

	public String toString(){
		String connective = "";
		switch (con){
		case AND: connective = " ∧ "; break;
		case OR: connective = " ∨ "; break;
		case IMPLIES: connective = " ⟶ "; break;
		}
		String out = "";
		String delim = "";
		for (Expression f : formulas){
			String f1 = (f instanceof AtomicFormula || f instanceof LambdaApplication)? f.toString(): new String("(" + f + ")");
			out = out + delim + f1;
			delim = connective;
		}
		return out;	
	}


	public FormulaList replace(Variable var, Expression e){
		ArrayList<Expression> newFormulas = new ArrayList<Expression>();
		for (Expression f : formulas){
			Expression f1 = f.replace(var, e);
			newFormulas.add(f1);
		}
		return new FormulaList(this.con, newFormulas);
	}


	public boolean equals(Object o){// strict equality (including order of terms), not equivalence 
		if (o != null && o instanceof FormulaList){
			FormulaList f = (FormulaList)o;

			if (this.con.equals(f.con) && this.formulas.size() == f.formulas.size()){
				for (int i = 0; i < this.formulas.size(); i++){
					Expression ei = this.formulas.get(i);
					Expression fi = f.formulas.get(i);
					if (!ei.equals(fi)){
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	public int hashCode(){
		int seed = 31;
		switch (con){
		case AND: seed *=1; break;
		case OR: seed *= 3; break;
		default: seed *= 5; break;
		}
		for (Expression f : formulas){
			seed += f.hashCode();
			seed *= 31;
		}
		return seed;
	}

	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat){
		if (e != null && e instanceof FormulaList){
			FormulaList f = (FormulaList)e;

			if (this.con.equals(f.con) && this.formulas.size() == f.formulas.size()){
				//boolean retval = true;
				for (int i = 0; i < this.formulas.size(); i++){
					Expression ei = this.formulas.get(i);
					Expression fi = f.formulas.get(i);
					if (ei.isEquivalentModuloVarRenaming(fi,varEquivalenceMapThis,varEquivalenceMapThat)){
						//retval = true;
					}
					else { 
						//retval = false;
						return false;
					}
				}
				return true;
			}
			else return false;
		}
		return false;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		String connective = "";
		switch (con){
		case AND: connective = "∧"; break;
		case OR: connective = "∨"; break;
		default: connective = ""; break;
		}
		String delim = null;
		for (Expression f : formulas){
			yield.addAll(f.yield());
			if (delim != null){
				yield.add(delim);
			}
			else {
				delim = connective;
			}
		}
		return yield;
	}

	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		TerminalType conType = null;
		for (Expression f : formulas){
			yieldTypes.addAll(f.yieldTypes());
			if (conType != null){
				yieldTypes.add(conType);
			}
			else {
				conType = TerminalType.CONNECTIVE;
			}
		}
		return yieldTypes;
	}

	public HashSet<Variable> getAllVariables() {
		HashSet<Variable> varSet = new HashSet<Variable>();		
		for (Expression f: formulas){
			HashSet<Variable> fVars = f.getAllVariables();
			if (fVars != null){
				varSet.addAll(fVars);
			}
		}
		return varSet;		
	}

	protected Expression renameVar(Variable v, Variable v1) {
		ArrayList<Expression> newFormulas = new ArrayList<Expression>();
		for (Expression f : formulas){
			Expression f1 = f.renameVar(v, v1);
			newFormulas.add(f1);
		}
		return new FormulaList(this.con, newFormulas);
	}


	public Expression flattenNestedFormulas(){
		//System.out.println("FormulaList.flattenNestedFormulas");
		ArrayList<Expression> fList = new ArrayList<Expression>();

		for (Expression f : formulas){
			//System.out.println("\t " + f);
			
			Expression f1 = f.flattenNestedFormulas();
			//System.out.println("\t =>" + f1);
			//System.out.println("Connectives match? " + this.connective() + " " + f1.connective());
			if (this.connective() == f1.connective()){
				//System.out.println("YES! " + f1.getClass());
				if (f1 instanceof ComplexFormula){
					fList.add(((ComplexFormula)f1).formula1);
					fList.add(((ComplexFormula)f1).formula2);	
				}
				else if (f1 instanceof FormulaList){
					fList.addAll(((FormulaList)f1).formulas);
				}
			}
			else {
				fList.add(f1);
			}
		}

		return FormulaList.createFormulaList(this.connective(), fList);


	}
}
