package semantics.lambdaFOL;

import java.util.Arrays;
import java.util.LinkedList;

import semantics.lambdaFOL.Expression.Quantifier;
import semantics.lambdaFOL.Variable.Status;

public class ExpressionTree {

	enum Type {CONSTANT, TERM_VARIABLE, FORMULA_VARIABLE, PREDICATE, LAMBDA, LAMBDA_ABSTRACTION, LAMBDA_APPLICATION, ATOMIC_FORMULA, QUANTIFIED_FORMULA, COMPLEX_FORMULA, NEGFORMULA, CONNECTIVE, QUANTIFIER}

	Expression expression;
	ExpressionTreeNode parent;
	Type type;


	public static ExpressionTree parse(String[] tokens){
		LinkedList<String> tokenList = new LinkedList<String>(Arrays.asList(tokens));
		ExpressionTreeNode newTree = new ExpressionTreeNode();
		return newTree.parse(tokenList);
	}

	public void printWholeTree() {
		if (parent != null)
			parent.printWholeTree();
		else {
			System.out.println("WHOLE TREE: "  + this);
		}
	}
	protected ExpressionTree parse(LinkedList<String> tokens){
		return this;
	}

	public Expression toExpression() {
		return null;
	}

	public Variable toVariable() {
		return null;
	}
	
	public Variable toVariable(Variable.Status status) {
		return null;
	}
	public TermVariable toTermVariable() {
		return null;
	}
	
	public TermVariable toTermVariable(Status quantified) {
		return null;
	}

	public Quantifier toQuantifier() {
		String string = this.toString();
		if (string != null) {
			if (string.equals(Expression.FORALL))
				return Quantifier.FORALL;
			if (string.equals(Expression.EXISTS))
				return Quantifier.EXIST;
			if (string.equals(Expression.UNIQUE))
				return Quantifier.UNIQUE;
		}
		return null;
	}

	
	public Expression.Connective toConnective() {
		// TODO Auto-generated method stub
		return null;
	}

	public Formula toFormula() {
		// TODO Auto-generated method stub
		return null;
	}

	public static boolean isTermVariable(String token){
		if (token == null)
			return false;
		if (isEntityVariable(token) || isEventVariable(token) || isTemporalVariable(token)){
			return true;
		}
		else return false;
	}
	public static boolean isEntityVariable(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[uvwxyz][0-9]*\\Z"))
			return true;
		else return false;
	}
	public static boolean isEventVariable(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[es][0-9]*\\Z")){
			return true;
		}
			else return false;
	}
	public static boolean isTemporalVariable(String token){
		if (token.matches("\\At[0-9]*\\Z"))
			return true;
		else return false;
	}
	public static boolean isConstant(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[abcdijklmnopqr][0-9]*\\Z") || 
				token.matches("\\A[a-zA-Z][a-zA-Z]+[0-9]*\\Z"))
			return true;
		else return false;
	}
	public static boolean isPredicate(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[abcdijklmnopqr][0-9]*\\Z") || 
				token.matches("\\A[a-zA-Z][a-zA-Z]+[0-9]*\\Z"))
			return true;
		else return false;
	}

	public static boolean isFormulaVariable(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[PQRSTfgh]\\Z"))
			return true;
		else return false;
	}

	public static boolean isVariable(String token){
		if (token == null)
			return false;
		if (isFormulaVariable(token) || isTermVariable(token))
			return true;
		else return false;
	}

	public static boolean isConnective(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[∧∨⟶]\\Z"))//hardcoded
			return true;
		else return false;
	}
	public static boolean isQuantifier(String token){
		if (token == null)
			return false;
		if (token.matches("\\A[∀∃ι]\\Z"))//hardcoded
			return true;
		else return false;
	}
	public static boolean isLambda(String token){
		if (token == null)
			return false;
		if (token.equals(Expression.LAMBDA))
			return true;
		else return false;
	}

	public static boolean isNegation(String token){
		if (token == null)
			return false;
		if (token.equals(Expression.NEG))
			return true;
		else return false;
	}

	
	
	

}
