package semantics.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class QuantifiedFormula extends Formula {

	protected final Quantifier quant;
	protected final TermVariable var;
	protected final Expression body; //formula or lambdaabstraction
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	public QuantifiedFormula(Quantifier q, TermVariable v, Expression b){
		quant = q;
		var = v;
		body = b;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}

	public String toString(){
		String q = EXISTS;
		switch (quant){
		case FORALL:
			q = FORALL;
			break;
		case EXIST:
			q = EXISTS;
			break;
		case UNIQUE:
			q = UNIQUE;
			break;
		}
		String f = (body instanceof AtomicFormula)? body.toString(): "(" + body.toString() + ")";
		return q + var.toString() + f;
	}

	public QuantifiedFormula replace(Variable v, Expression e){
		if (var.equals(v))
			return this;
		Formula body2 = (Formula)body.replace(v, e);
		return new QuantifiedFormula(quant, var, body2);
	}
	
	public Expression flattenNestedFormulas(){
		Formula bodyFlat = (Formula)body.flattenNestedFormulas();
		return new QuantifiedFormula(quant, var, bodyFlat);
	}

	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat) {
		if (e != null && e instanceof QuantifiedFormula){
			QuantifiedFormula f = (QuantifiedFormula)e;

			//System.out.println("QUANTIFIED FORMULA EQUIVALENCE CHECK: " + this.quant + " " + f.quant + " ( " + this.quant.equals(f.quant) + " ) " + this.body  + " " + f.body);
			//System.out.println("This map: " + varEquivalenceMapThis.keySet());
			//for (Variable v : varEquivalenceMapThis.keySet()){
			//	System.out.println(v + " -> " + varEquivalenceMapThis.get(v));
			//}
			//System.out.println("That map: " + varEquivalenceMapThat.keySet());
			//for (Variable v : varEquivalenceMapThat.keySet()){
			//	System.out.println(v + " -> " + varEquivalenceMapThat.get(v));
			//}
			if (this.quant.equals(f.quant)){
				Variable x = varEquivalenceMapThis.get(this.var);
				Variable y = varEquivalenceMapThat.get(f.var);
				//System.out.println(this.var + " -> x: "  + x );
				//System.out.println(f.var + " -> y: "  + y );

				if (x == null && y == null){
					varEquivalenceMapThis.put(this.var, f.var);
					varEquivalenceMapThat.put(f.var, this.var);
					return this.body.isEquivalentModuloVarRenaming(f.body, varEquivalenceMapThis, varEquivalenceMapThat);
				}
				// TODO: I'm not sure we want this clause (see also LambdaAbstraction)
				else if (x != null && y != null && this.var.equals(y) && f.var.equals(y) ){// there was a typo: used to by this.equals(y)
					return this.body.isEquivalentModuloVarRenaming(f.body, varEquivalenceMapThis, varEquivalenceMapThat);
				}
				else return false;
			}
			else return false;
		}
		return false;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(quant.toString());
		yield.add(var.toString());
		yield.addAll(body.yield());
		return yield;
	}
	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.add(TerminalType.QUANTIFIER);
		yieldTypes.add(TerminalType.TERMVARIABLE);
		yieldTypes.addAll(body.yieldTypes());
		return yieldTypes;
	}
	
	public HashSet<Variable> getAllVariables() {
		HashSet<Variable> varSet = new HashSet<Variable>();
		varSet.add(var);
		HashSet<Variable> bodyVars = body.getAllVariables();
		if (bodyVars != null){
			varSet.addAll(bodyVars);
		}
		return varSet;		
	}
	protected Expression renameVar(Variable v, Variable v1) {
		TermVariable v2 = (v instanceof TermVariable && v1 instanceof TermVariable && this.var.equals(v))?(TermVariable)v1:this.var;
		Expression body1 = body.renameVar(v, v1);
		return new QuantifiedFormula(this.quant,v2, body1);
	}
}
