package semantics.lambdaFOL;

import java.util.HashMap;

/* 
 * Terms refer to entities.
 * We assume different sorts of entities: actual entities, times, events (plural entities?, time intervals?) 
 */
public class Term extends Expression {

	public String name(){
		return toString();
	}
	
	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat) {
	 return false;
	}
}
