package semantics.lambdaFOL;

public class ExpressionLeaf extends ExpressionTree {
	String string;

	ExpressionLeaf(String s, ExpressionTreeNode p){
		if (isTermVariable(s)){
			expression = TermVariable.createTermVariable(s);
			type = Type.TERM_VARIABLE;
		}
		else if (isFormulaVariable(s)){
			expression = FormulaVariable.createFormulaVariable(s);
			type = Type.FORMULA_VARIABLE;
		}
		else if (isConstant(s)){
			expression = Constant.createConstant(s);
			type = Type.CONSTANT;
		}
		else if (isLambda(s)){
			type = Type.LAMBDA;
			expression = Expression.parseToken(string);// TODO: LAMBDA ABSTRACTIONS ARE NOW SINGLE TOKENS  " λ x f ( x ) "
		}
		else if (isQuantifier(s)){
			type = Type.QUANTIFIER;
			expression  = Expression.parseToken(string);// TODO
		}
		//string = (type == Type.LAMBDA)?"λ"+ expression.toString():expression.toString();	
		parent = p;
		if (parent != null)
			p.addChild(this);
	}
	ExpressionLeaf(Type t, String s, ExpressionTreeNode p){
		type = t;// this assumes the type matches the expression
		expression = Expression.parseToken(s);// either a variable or a constant -- or a predicate?? TODO
		string = (expression != null)?expression.toString():s;
		parent = p;
		if (parent != null)
			p.addChild(this);
	}

	public String toString(){
		return new String(string);
	}
	
	public Variable toVariable() {
		Variable var = null;
		switch (type){
		case TERM_VARIABLE:
			var = (TermVariable)expression;
			break;
		case FORMULA_VARIABLE:
			var = (FormulaVariable)expression;
			break;
		case LAMBDA:		
			var = (Variable)expression;
			//System.out.println("ExpressionLeaf.toVariable(): lambda " + string + " -> " + var);
			break;
		default:
				System.out.println("ExpressionLeaf.toVariable() ERROR: this is not a variable " + this); 
		}
		return var;
	}
	
	public TermVariable toTermVariable(){
		TermVariable var = null;
		if (this.type == Type.TERM_VARIABLE){
			var = (TermVariable)expression;
		}
		return var;
	}
	public Formula toFormula(){
		Formula f = null;
		if (type == Type.FORMULA_VARIABLE){
			f = FormulaVariable.createFormulaVariable(string);//TODO: debug me!
		}
		return f;
	}
	public Expression.Connective toConnective(){
		if (type == Type.CONNECTIVE){
			if (string.equals(Expression.AND)){
				return Expression.Connective.AND;
			}
			else if (string.equals(Expression.OR)){
				return Expression.Connective.OR;
			}
			else if (string.equals(Expression.IMPLIES)){
				return Expression.Connective.IMPLIES;
			}
		}
		return null;
	}
	
	
	public Expression toExpression(){
		Expression e = null;
		switch (type){
		case CONSTANT:
			e = new Constant(string);
			break;
		case TERM_VARIABLE:
			e = expression;
			//System.out.println("CHECK " + e + " " + var2);
			break;
		case FORMULA_VARIABLE:
			e = FormulaVariable.createFormulaVariable(string);
			break;
		case PREDICATE:
			System.out.println("ExpressionLeaf.toExpression() TODO: predicate " + string);
			break;
		case LAMBDA:
			System.out.println("ExpressionLeaf.toExpression() TODO: lambda " + string);
			break;
		case CONNECTIVE:
			System.out.println("ExpressionLeaf.toExpression() TODO: connective " + string);
			break;
		default:
			System.out.println("ExpressionLeaf.toExpression() ERROR: this should not be a leaf " + this); 
		}
		return e;
	}
}
