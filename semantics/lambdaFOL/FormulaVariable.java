package semantics.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class FormulaVariable extends Formula implements Variable {
	static int counter = 0;
	final Status status;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	static HashMap<String,FormulaVariable> stringToVar = new HashMap<String,FormulaVariable>();
	public static void resetCounters(){
		counter = 0;
	}

	final String string;
	final int index;

	protected static String statusString(Status s){
		String statusString = null;		
		if (s == Status.QUANTIFIED){
			statusString = "(Q)";
		}
		else if (s == Status.LAMBDABOUND){
			statusString = "(B)";
		}
		else if (s == Status.FREE){
			statusString = "(F)";
		}
		return statusString;
	}
	protected FormulaVariable(){
		index = ++counter;
		status = Status.FREE;
		string = computeString();	
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	
	protected String computeString(){
		String statusString = "";//statusString(status);
		return new String("P" + index + statusString);
	}
	protected FormulaVariable(Status s){
		index = ++counter;
		status = s;
		string = computeString();
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	
	public FormulaVariable(FormulaVariable var) {
		this.index = var.index;
		this.string = var.string;
		this.status = var.status;
		this.yield = var.yield();//TODO: copy
		yieldTypes = var.yieldTypes();//TODO: copy
	}
	public FormulaVariable(FormulaVariable var, Status status) {
		this.index = var.index;
		this.status = status;
		string = computeString();
		yield = var.yield();//TODO: copy
		yieldTypes = var.yieldTypes();//TODO: copy
	}
	
	public boolean equals(Object o){
		if (o instanceof FormulaVariable){
			FormulaVariable v = (FormulaVariable)o;
			if (v.index == this.index
					&& v.string.equals(v.string)){ 
				return true;
			}
			else return false;
		}
		else return false;
	}

	public int hashCode(){
		int seed = 31 * string.hashCode();
		seed += index;
		return seed;
	}


	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat) {
		if (e != null && e instanceof FormulaVariable){
			FormulaVariable v = (FormulaVariable)e;
			Variable x = varEquivalenceMapThis.get(this);
			Variable y = varEquivalenceMapThat.get(v);
			if (x == null && y == null){
				varEquivalenceMapThis.put(this, v);
				varEquivalenceMapThat.put(v, this);
				return true;
			}
			else if (x != null && y != null && this.equals(y) && v.equals(x)){
				return true;
			}
			else return false;
		}
		return false;
	}

	@Override
	public String name() {
		return string;
	}

	public String toString(){
		return string;
	}

	@Override
	public VarType type() {
		return VarType.FORMULA;
	}

	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(this.toString());
		return yield;
	}
	
	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.add(TerminalType.FORMULAVARIABLE);
		return yieldTypes;
	}

	public static FormulaVariable createFormulaVariable(String string) {
		FormulaVariable var = stringToVar.get(string);
		//System.out.println("\tDEBUG FormulaVariable.createFormulaVariable: Input string: " + string + " var: " + var );
		if (var != null){

			FormulaVariable varNew = new FormulaVariable(var);
			//System.out.println("\tDEBUG FormulaVariable.createFormulaVariable: copied the old variable: " + var +  " -> " + varNew);
			return varNew;
		}
		else {
			var = new FormulaVariable();
			//System.out.println("\tDEBUG FormulaVariable.createFormulaVariable: created a new variable: " + var + "  var.string: " + var.string + " input string: " + string  );
			stringToVar.put(var.string, var);
			stringToVar.put(string, var);
			return var;
		}
	}

	public static FormulaVariable createFormulaVariable(String string, Variable.Status status) {
		FormulaVariable var = stringToVar.get(string);
		//System.out.println("DEBUG FormulaVariable.createFormulaVariable: Input string: " + string + " var: " + var );
		if (var != null){
			FormulaVariable varNew = new FormulaVariable(var, status);
			if (status != var.status()){
				stringToVar.put(string, varNew);
			}
			//System.out.println("DEBUG FormulaVariable.createFormulaVariable: copied the old variable: " + var +  " -> " + varNew);
			return varNew;
		}
		else {
			var = new FormulaVariable(status);
			//System.out.println("DEBUG FormulaVariable.createFormulaVariable: created a new variable: " + var + "  var.string: " + var.string + " input string: " + string  );
			//stringToVar.put(var.string, var);
			stringToVar.put(string, var);
			return var;
		}
	}
	public Expression replace(Variable var, Expression e){
		if (this.equals(var)){
			return e;
		}
		else return this;
	}
	public static void resetStringToVarMap() {
		stringToVar = new HashMap<String,FormulaVariable>();	
	}

	public HashSet<Variable> getAllVariables() {
		HashSet<Variable> varSet = new HashSet<Variable>();
		varSet.add(this);
		return varSet;		
	}
	
	public Variable getNewVariable(){
		return new FormulaVariable();
	}
	protected Expression renameVar(Variable v, Variable v1) {
		if (this.equals(v) && v1 instanceof FormulaVariable)
			return new FormulaVariable((FormulaVariable)v1);
		else return this;
	}
	public Status status(){
		return status;
	}
}


