package semantics.lambdaFOL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class AtomicFormula extends Formula implements Comparable<AtomicFormula>{

	final public String pred;
	public Expression[] args;
	final public ArrayList<String> yield;
	final public ArrayList<TerminalType> yieldTypes;
	
	// pass in the predicate and all arguments
	public AtomicFormula(String p, Expression[] a){
		pred = p;
		args = a;
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	
	
	// pass in the predicate and the number of arguments, to be instantiated with entity variables
	public AtomicFormula(String p, int n){
		pred = p;
		args = new Expression[n];
		for (int i = 0; i <n; i++){
			args[i] = new TermVariable(VarType.ENTITY, Variable.Status.FREE);
		}
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	
	public AtomicFormula(String p, VarType[] argTypes, Variable.Status[] status){
		pred = p;
		args = (argTypes == null)?null:new Expression[argTypes.length];
		if (args != null){
			for (int i = 0 ; i < args.length; i++){
				args[i] = (Expression) new TermVariable(argTypes[i], status[i]);				
			}
		}
		yield = initYield();
		yieldTypes = initYieldTypes();
	}
	

	public AtomicFormula replace(Variable var, Expression e){
		Expression[] args2 = new Expression[args.length];
		for (int i = 0; i < args.length; i++){
			args2[i] = args[i].replace(var, e);
		}
		return new AtomicFormula(pred, args2);		
	}
	
	public String toString(){
		String res = pred + "(";
		for (int i = 0; i < args.length; i++){
			res = res.concat(args[i].toString());
			if (i < args.length -1){
				res = res.concat(", ");
			}
		}
		res = res.concat(")");
		return res;
	}

	public int arity(){
		return args.length;
	}
	
	
	
	/* 
	 * @return the yield of this atomic formula. A predicate "pred" with N arguments gets turned into "pred_n=N"
	 */
	protected ArrayList<String> initYield(){
		ArrayList<String> yield = new ArrayList<String>();
		yield.add(pred  + "_N=" + args.length); 
		if (args != null){
			for (int i = 0 ; i < args.length; i++){
				yield.addAll(args[i].yield());	
			}
		}
		return yield;
	}
	
	public ArrayList<String> yield(){
		return this.yield;
	}
	public ArrayList<TerminalType> yieldTypes(){
		return this.yieldTypes;
	}
	
	protected ArrayList<TerminalType> initYieldTypes(){
		ArrayList<TerminalType> yieldTypes = new ArrayList<TerminalType>();
		yieldTypes.add(TerminalType.PREDICATE);
		if (args != null){
			for (int i = 0 ; i < args.length; i++){
				yieldTypes.addAll(args[i].yieldTypes());	
			}
		}
		return yieldTypes;
	}
	
	/* (non-Javadoc)
	 * @see lambdaFOL.Expression#isEquivalentModuloVarRenaming(lambdaFOL.Expression, java.util.HashMap, java.util.HashMap)
	 */
	protected boolean isEquivalentModuloVarRenaming(Expression e, HashMap<Variable, Variable> varEquivalenceMapThis, HashMap<Variable, Variable> varEquivalenceMapThat){
		if (e instanceof AtomicFormula){
			//System.out.println("Checking " + this + "  =?= " + e);
			AtomicFormula a = (AtomicFormula)e;
			if (this.pred.equals(a.pred) && this.arity() == a.arity()){
				for (int i = 0; i < this.arity(); i++){
					//System.out.println("Comparing: " + this.args[i] + " and " + a.args[i] + " " + this.args[i].getClass() + " " + a.args[i].getClass());
					//System.out.println("Atomic formula equivalence check: comparing " + this.args[i] + " (" + varEquivalenceMapThis.get((Variable)this.args[i]) + ") and  " + a.args[i] + " (" + varEquivalenceMapThat.get((Variable)a.args[i]) + ") " + equivalenceMapThis.keySet() + " " + equivalenceMapThat.keySet());
					if (!this.args[i].isEquivalentModuloVarRenaming(a.args[i],varEquivalenceMapThis,varEquivalenceMapThat)){
						//System.out.println("   Atomic formula equivalence check: just compared  (and failed) ");// + this.args[i] + " (-> " + varEquivalenceMapThis.get((Variable)this.args[i]) + ") and  " + a.args[i] + " (-> " + equivalenceMapThat.get((Variable)a.args[i]) + ")" );
						return false;
					}
				//	else {
						//System.out.println("   Atomic formula equivalence check: just compared  (and passed) ");// + this.args[i] + " (-> " + varEquivalenceMapThis.get((Variable)this.args[i]) + ") and  " + a.args[i] + " (-> " + equivalenceMapThat.get((Variable)a.args[i]) + ")" );
					//}
				}
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public HashSet<Variable> getAllVariables() {
		HashSet<Variable> varSet = new HashSet<Variable>();
		for (Expression e: args){
			HashSet<Variable> eVars = e.getAllVariables();
			if (eVars != null)
				varSet.addAll(eVars);
		}
		return varSet;		
	}
	
	
	protected Expression renameVar(Variable v, Variable v1) {
		Expression[] newArgs = new Expression[args.length];
		for (int i = 0; i < args.length; i++){
			newArgs[i] = args[i].renameVar(v, v1);
		}
		return new AtomicFormula(this.pred, newArgs);
	}


	@Override
	public int compareTo(AtomicFormula f) {
		if (f == null)
			return 1;
		if (f.pred == null && this.pred != null)
			return 1;
		if (f.pred == null && this.pred == null)
			return 0;
		//System.out.println("DEBUG compare to: " + this.pred + " " + f.pred + " " + this.pred.compareTo(f.pred));
		return this.pred.compareTo(f.pred);// TODO: fix bug: this returns 0 even if the two formulas are not equal
	}
	

	
}
