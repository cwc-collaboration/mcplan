package generator;

public class Action {
	protected Predicate predicate;
	protected String[]  arguments;
	
	/**
	 * Constructor for a default action with no predicate
	 * and no specified arguments.
	 */
	public Action() {
		predicate = Predicate.MOVE;
		arguments = new String[3];
	}
	
	/**
	 * Constructor for an action with a specified predicate
	 * and no specified arguments.
	 * @param pred The predicate for this action
	 */
	public Action(Predicate pred) {
		predicate = pred;
		arguments = new String[3];
	}
	
	/**
	 * Constructor for an action with a specified predicate
	 * and specified arguments. 
	 * @param pred The predicate for this action
	 */
	public Action(Predicate pred, String[] args) {
		predicate = pred;
		arguments = args;
		
		if (args.length < 3) {
			arguments = new String[3];
			
			for (int i = 0; i < args.length; i++) arguments[i] = args[i];
			for (int i = args.length; i < arguments.length; i++) arguments[i] = null;
		}
	}
	
	// accessor methods
	public Predicate getPredicate() { return predicate; }
	public String[]  getArguments() { return arguments; }
	
	// accessor methods for specific arguments
	public String getEntity()      { return (arguments.length >= 1 ? arguments[0] : null); }
	public String getDestination() { return (arguments.length >= 2 ? arguments[1] : null); }
	public String getSource()      { return (arguments.length >= 3 ? arguments[2] : null); }
	
	// mutator methods for specific arguments
	public void setEntity(String e) 	 { arguments[0] = e; }
	public void setDestination(String d) { arguments[1] = d; }
	public void setSource(String s) 	 { arguments[2] = s; }
	
	/**
	 * Checks if any arguments have been specified for this action.
	 * @return True if no arguments exist
	 */
	public boolean emptyArguments() {
		boolean ret = true;
		for (String arg : arguments) {
			if (arg != null) ret = false;
		}
		return ret;
	}
}
