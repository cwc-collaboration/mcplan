package generator;

import semantics.ccg.Normal_Form;
import semantics.generation.Configuration;

import java.util.ArrayList;

/**
 * Prints out some sample output (expand this as we go).
 * @author ramusa2
 *
 */
public class FreeCellGeneratorSample {
	
	public static void testSuite() {
		String[] obj1 = {"the queen of hearts"};
		String[] obj2 = {"the queen of hearts", "the freecell"};
		String[] obj3 = {"the queen of hearts", "the freecell", "the stack"};
		
		String[] obj4 = {"the queen of hearts", "the freecell", "the seventh stack"};
		String[] obj5 = {"the seven of diamonds"};
		String[] obj6 = {"the ace of spades"};
		String[] obj7 = {"the two of clubs"};
		
		Action m0   = new Action(Predicate.MOVE); 	   	   
		Action m1   = new Action(Predicate.MOVE, obj1); 
		Action m2   = new Action(Predicate.MOVE, obj2); 
		Action m3   = new Action(Predicate.MOVE, obj3); 
		Action m4   = new Action(Predicate.MOVE, obj4); 
		
		Action f0   = new Action(Predicate.FINISH);
		Action f1   = new Action(Predicate.FINISH, obj3);
		Action f2   = new Action(Predicate.FINISH, obj5);
		
		Action ffc0 = new Action(Predicate.FINISH_FROM_CELL);
		Action ffc1 = new Action(Predicate.FINISH_FROM_CELL, obj1);
		
		Action fr0  = new Action(Predicate.FREE);
		Action fr1  = new Action(Predicate.FREE, obj1);
		Action fr2  = new Action(Predicate.FREE, obj6);
		
		Action u0   = new Action(Predicate.UNFREE);
		Action u1   = new Action(Predicate.UNFREE, obj1);
		Action u2   = new Action(Predicate.UNFREE, obj7);
		
		// Report action applied
		System.out.println("\n*** Action applied ***");
		System.out.println(Gen.actionApplied("finish the six of clubs"));
		
		// Ambiguous target
		System.out.println("\n*** Ambiguous target***"); 
		System.out.println(Gen.ambiguousTarget("seven", "the seven of hearts", "the seven of diamonds"));
		System.out.println(Gen.ambiguousTarget("ace", "the ace of hearts", "the ace of diamonds", "the ace of spades"));
		
		// Invalid move
		System.out.println("\n*** Invalid move ***"); 
		System.out.println(Gen.invalidMove());
		
		// Report move actions
		System.out.println("\n*** Move actions ***"); 
		System.out.println(Gen.movePerformed(m0));
		System.out.println(Gen.movePerformed(m1));
		System.out.println(Gen.movePerformed(m2));
		System.out.println(Gen.movePerformed(m3));
		
		System.out.println("\n*** Finish actions ***"); 
		System.out.println(Gen.movePerformed(f0));
		System.out.println(Gen.movePerformed(f1));
		
		System.out.println("\n*** Finish-from-cell actions ***"); 
		System.out.println(Gen.movePerformed(ffc0));
		System.out.println(Gen.movePerformed(ffc1));
		
		System.out.println("\n*** Free actions ***"); 
		System.out.println(Gen.movePerformed(fr0));
		System.out.println(Gen.movePerformed(fr1));
		
		System.out.println("\n*** Unfree actions ***"); 
		System.out.println(Gen.movePerformed(u0));
		System.out.println(Gen.movePerformed(u1));
		
		// Produce move commands
		System.out.println("\n*** Move commands ***"); 
		System.out.println(Gen.moveCommand(m0));
		System.out.println(Gen.moveCommand(m1));
		System.out.println(Gen.moveCommand(m2));
		System.out.println(Gen.moveCommand(m3));
		
		System.out.println("\n*** Finish commands ***"); 
		System.out.println(Gen.moveCommand(f0));
		System.out.println(Gen.moveCommand(f1));
		
		System.out.println("\n*** Finish-from-cell commands ***"); 
		System.out.println(Gen.moveCommand(ffc0));
		System.out.println(Gen.moveCommand(ffc1));
		
		System.out.println("\n*** Free commands ***"); 
		System.out.println(Gen.moveCommand(fr0));
		System.out.println(Gen.moveCommand(fr1));
		
		System.out.println("\n*** Unfree commands ***"); 
		System.out.println(Gen.moveCommand(u0));
		System.out.println(Gen.moveCommand(u1));
		
		ArrayList<Action> seq1 = new ArrayList<Action>();
		seq1.add(m4);
		seq1.add(f2);
		
		ArrayList<Action> seq2 = new ArrayList<Action>();
		seq2.add(m4);
		seq2.add(f2);
		seq2.add(ffc1);
		seq2.add(fr2);
		seq2.add(u2);
		
		// Move preference
		System.out.println("\n*** Move preference ***");
		System.out.println(Gen.movePreference(new ArrayList<Action>()));
		System.out.println(Gen.movePreference(seq1));
		System.out.println(Gen.movePreference(seq2));
		
		// Bad reward
		System.out.println("\n*** Bad reward ***");
		System.out.println(Gen.badReward(new ArrayList<Action>()));
		System.out.println(Gen.badReward(seq1));
		System.out.println(Gen.badReward(seq2));
		
		System.out.println("\n*** Subtasks ***");
		System.out.println(Gen.subtask(Subtask.OUT_OF_THE_WAY, "the ace of hearts", "the queen of hearts"));     
		System.out.println(Gen.subtask(Subtask.BOTTOM_OUT_OF_THE_WAY, "the seven of diamonds")); 
		System.out.println(Gen.subtask(Subtask.DO_CARD, "the two of hearts"));				
		
		// Logical form generation (placeholder)
		System.out.println("\n*** Logical form ***"); 
		System.out.println(Gen.logicalForm("ιx2248(card(x2248) ∧ ιx269(stack(x269) ∧ (destination(x269) ∧ moved(x2248, x269) ∧ patient(x2248))))"));
	}

	public static void main(String[] args) {
		System.out.println("Sample communications (dummy/template-based):");
		Gen.initializeDummyGenerator();
		testSuite();
		
		Configuration config = new Configuration();
		config.NF    = Normal_Form.Full;
		config.DEBUG = false;
		Gen.initializeSemanticGenerator("data/semantics/testlexicon.txt");
		testSuite();
	}
	
}
