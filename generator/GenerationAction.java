package generator;

/**
 * List of flags for FreeCell communication events.
 * 
 * @author ramusa2
 *
 */
public enum GenerationAction {
	
	ACTION_APPLIED,
	
	AMBIGUOUS_TARGET,
	
	INVALID_MOVE,
	
	MOVE_PERFORMED,
	
	MOVE_COMMAND,
	
	MOVE_PREFERENCE,
	
	BAD_REWARD,
	
	LOGICAL_FORM

}
