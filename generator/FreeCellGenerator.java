package generator;

import java.util.Collection;

/**
 * The FreeCellGenerator interface defines the method signatures
 * for the various supported communication actions.
 * 
 *  The initial draft of this class includes some sample responses
 *  for a game of FreeCell -- this set isn't meant to be complete, 
 *  and it should be added to or modified as needed.
 * 
 * @author ramusa2
 *
 */
public interface FreeCellGenerator {
	
	/**
	 * Reports the list of actions performed.
	 * 
	 * @param parameters	a list of actions performed
	 */
	public abstract String actionApplied(String... actions);
	
	/**
	 * Reports that an entity referenced by the user is ambiguous, and
	 * lists the potential matches.
	 * 
	 * @param parameters	the proposed entity, followed by possible matches.
	 */
	public abstract String ambiguousTarget(String originalEntity, String... possibleMatches);


	/**
	 * Returns a canned response indicating that the user's proposed move is invalid.
	 */
	public abstract String invalidMove();
	
	/**
	 * Reports a move event to the user.
	 * 
	 * 
	 * @param action		the action in this move event
	 * @return				the report of the move event
	 */
	public abstract String movePerformed(Action action);
	
	/**
	 * Returns a move command to the user.
	 * 
	 * 
	 * @param action		the action in this move command
	 * @return				the move command
	 */
	public abstract String moveCommand(Action action);

	/**
	 * Indicates a need for move preference from the user, then lists (zero or more) 
	 * suggestions for move commands to take.
	 * 
	 * @param actions		possible actions to take
	 * @return				the move preference request
	 */
	public abstract String movePreference(Collection<Action> actions);
	
	/**
	 * Indicates that the user's last suggestion was a bad move, then lists (zero or more)
	 * suggestions for move commands to take.
	 * 
	 * @param action		possible actions to take
	 * @return				the bad move request
	 */
	public abstract String badReward(Collection<Action> actions);
	
	/**
	 * Returns a realization of the given subtask.
	 * 
	 * @param s				the type of subtask
	 * @param parameters	parameters of the subtask
	 * @return				the subtask description
	 */
	public abstract String subtask(Subtask s, String... parameters);

	/**
	 * Converts a logical form (semantic predicate in lambda syntax)
	 * into a natural-language string.	
	 */
	public abstract String logicalForm(String logicalForm);

}
